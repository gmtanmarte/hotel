<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin.php");


session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

 $getData = getAllAdmin();
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
	<head>
        <title>Employee Salary</title>
        <script src="../scripts/jquery.js"></script>
      
  </head>
  <script type="text/javascript">

        function printThis(){
            window.print();
        }


    function getType(str){

          getTotal();
    }

function getTotal(id){
    //var c = this.id;
    var a = "days"+id;
    var b= "sal"+id;

    var c = document.getElementById(a).value;
    var d = document.getElementById(b).value;
    var ans = 'total'+id;
    var totalAns = document.getElementById(ans).value = (c*d);

    
}

  </script>
  <style type="text/css">
     
     @media print {
        body * {
          visibility: hidden;
         }
      
      #section-to-print, #section-to-print * {
        visibility: visible;
      }
  
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0; 
      }
} 


  </style>

<body>

  <div style="width: 100%; padding-top: 7%;"> 
   <center>
    <form method="post" onsubmit="printThis()" >
       <table id="section-to-print">
         <th>ID</th>
         <th>Name</th>
         <th>No. Of Days</th>
         <th>Salary Per Day</th>
         <th>Total</th>
          <tbody>
         <?php
         foreach ($getData as $row) {?>
             <tr>

                 <td><center><?php echo $idUser = $row['user_id'];?></center> </td>
                 <td> <b> <?php echo $row['fname']." ".$row['mname']." ".$row['lname'];?></b></td>      
                 <td> <center><input type="text" name="days<?php echo $idUser; ?>" id="days<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required> </center> </td>
                 <td> <center><input type="text" name="sal<?php echo $idUser; ?>" id="sal<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required> </center></td>
                 <td> <center><b> <input type="text" name="total<?php echo $idUser;?>" id="total<?php echo $idUser;?>"  size="5px" readonly="" /></b></center></td>
            </tr> <?php } ?>
          </tbody>
        </table>
      
       </center>
    <div style="float: right; margin-right: 10%; margin-top: 5%;">
      <button style="font-size: 18px;" class="button" name="saveSalaryBTN" >Preview</button>
    </div>
  </form><br> 
    </div>
</body>
</html>
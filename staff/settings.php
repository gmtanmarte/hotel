<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

  $getData = getRoomType();
  $getData1 = getRoom();

 $url = $_SERVER['REQUEST_URI'];
?>

<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
        <title>Admin's Page</title>
        <script src="../scripts/jquery.js"></script>
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>

<div id="roomTypeID" style="width: 100%; padding-top: 2%;">
<center>
   <h2 >Add Room Type</h2>
   <br>

    <table  class="table">
      <thead class="thead-dark">
          <th  scope="col">Room Type</th>
          <th  scope="col">Room Price</th>
          <th  scope="col">Total No Of Rooms</th>
          <th  scope="col">Photo</th>
          <th  scope="col">Action</th>
      </thead>
      
        <tr>
          <form method="post" enctype="multipart/form-data" >
            <td><input type="text" name="roomType" class="form-control" required></td>
            <td><input type="text" name="price" class="form-control" required></td>
            <td><input type="number" min="0" max="10" name="noOfRooms" class="form-control" require></td>
            <td><input type="file" name="image" id="image" multiple required></td>
            <td><button class="btn btn-success" name="saveRoomTypeBTN">Add</button></td>
          </form>
        </tr>
   </table>
   <br><br>
    
    <table class="table" style="width: 90%;">
      <thead class="thead-dark">
        <th  scope="col">Room Type</th>
        <th  scope="col">Room Price</th>
        <th  scope="col">Total No Of Rooms</th>
        <th  scope="col">Action</th>
      </thead>
        <?php  foreach ($getData as $row) {?>
         <tr>
            <td> <?php echo $row['typeName'];?> </td>
            <td> <?php echo $row['price'];?> </td>
            <td> <?php echo $row['noOfRooms'];?></td>
            <td> <?php echo '<a style="color:black;" href="editRoomType.php?id='. $row['id'].'">Update</a>';?> </td>
        </tr> <?php } ?>
      </table>
 </div>
<br><br><br>


 <br><br><br><br><br>
</body>
</html>
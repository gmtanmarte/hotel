<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

 $getData = getAllAdmin();
 
?>
<!DOCTYPE html>
<meta charset = "eng">
<html>
<head>
	
        <title>Admin's Page</title>
        <script src="../scripts/jquery.js"></script>
        <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
    </head>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>

  <div style="width: 100%; padding-top:2%;"> 
   <center>
   <h2>Employee list</h2>
   <br>
    <form method="post">
       <table class="table">
       <thead class="thead-dark">
         <th scope="col">ID</th>
         <th scope="col">First Name</th>
         <th scope="col">Middle Name</th>
         <th scope="col">Last Name</th>
         <th scope="col">Username</th>
         <th scope="col">Email</th>
         <th scope="col">Contact</th>
         <th scope="col">Action</th>
         </tr>
  </thead>
          <tbody>
         <?php
         foreach ($getData as $row) {?>
             <tr>
                 <td><center><?php echo $row['user_id'];?></center> </td>
                 <td> <?php echo $row['fname'];?> </td>
                 <td> <?php echo $row['mname'];?></td>      
                 <td> <?php echo $row['lname'];?></td>         
                 <td> <?php echo $row['username']; ?> </td>
                 <td> <?php echo $row['email']; ?> </td>
                 <td> <?php echo $row['contact']; ?> </td>
                 <td> <?php echo '<a  style="font-size: 12px;" href="editEmployee2.php?id='. $row['user_id'].'">edit</a>';?> </td>
            </tr> <?php } ?>
          </tbody>
        </table>
        </form>
       </center><br>
    </div>

    <div style="float: right; margin-right: 1%;">
      <form method="post">
      <button onclick="location.href = 'generateSalary2.php'" type="button" class="btn btn-light" style="background-color: #efe786;" >Employee Salary</button>
     </form>
    </div>
</body>
</html>
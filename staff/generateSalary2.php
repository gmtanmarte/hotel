<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");


session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

 $getData = getAllAdmin();
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
	<head>
        <title>Employee Salary</title>
        <script src="../scripts/jquery.js"></script>
        
        
  </head>
  <script type="text/javascript">

        function printThis(){
            window.print();
        }


    function getType(str){

          getTotal();
    }

function getTotal(id){
    //var c = this.id;
    var a = "days"+id;
    var b= "sal"+id;

    var c = document.getElementById(a).value;
    var d = document.getElementById(b).value;
    var ans = 'total'+id;
    var totalAns = document.getElementById(ans).value = (c*d);

    
}

  </script>
  <style type="text/css">
     
     @media print {
        body * {
          visibility: hidden;
         }
      
      #section-to-print, #section-to-print * {
        visibility: visible;
      }
  
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
     
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

} 


  </style>
 
<body>

  <div style="width: 100%; padding-top:2%;"> 
   <center>
   <h2>Generate Salary</h2>
   <br>
    <form method="post" onsubmit="printThis()" >
       <table class="table" id="section-to-print">
       <thead class="thead-dark">
         <th  scope="col">ID</th>
         <th  scope="col">Name</th>
         <th  scope="col">No. Of Days</th>
         <th  scope="col">Salary Per Day</th>
         <th  scope="col"   >Total</th>
          <tbody>
         <?php
         foreach ($getData as $row) {?>
             <tr>

                 <td><center><?php echo $idUser = $row['user_id'];?></center> </td>
                 <td> <b> <?php echo $row['fname']." ".$row['mname']." ".$row['lname'];?></b></td>      
                 <td> 
                 <input type="text" class="form-control col-md-4"  name="days<?php echo $idUser; ?>" id="days<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required> </center> 
                 </td>
                 <td> 
                 <div class="input-group mb-3">
                 <div class="input-group-prepend ">
                 <span class="input-group-text" id="basic-addon1">₱</span>
                </div>
                <input type="text" class="form-control col-md-3" name="sal<?php echo $idUser; ?>" id="sal<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required> </center>
                </div>
                </td>
                 <td> 
                 <div class="input-group mb-1">
                <div class="input-group-prepend">
                    <span class="input-group-text">$</span>
                </div>
                <input type="text" class="form-control col-md-5" name="total<?php echo $idUser;?>" id="total<?php echo $idUser;?>"  size="5px" readonly="" /></b></center>
                <div class="input-group-append">
                    <span class="input-group-text">.00</span>
                </div>
                </div>
                 
            </tr> <?php } ?>
          </tbody>
        </table>
      
       </center>
     <div style="float: right; margin-right: 1%; margin-top: 1%;">
     
      <button class="btn" style="background-color: #efe786;" name="saveSalaryBTN" >Preview</button>
    </div>
  </form><br> 
    </div>
</body>
</html>
  <?php
  include("../includes/config.php");
  include("../includes/query.php"); 
  include("../includes/button_function.php");
  include("../includes/headerAdmin2.php");

    $getData1 = getPosition();

   $url = $_SERVER['REQUEST_URI'];
  ?>

  <!DOCTYPE html>
  <meta charset = "eng">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
  <html>
  <head>
          <title>Admin's Page</title>
          <script src="../scripts/jquery.js"></script>
  </head>
  <style>
      @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
  html body {
    
      margin: 0;
      padding: 0;
      overflow-x: hidden;
      font-family: 'Montserrat', sans-serif;
      font-size: 100%;
      background-color: #EEEEEE;
      color: #333333;
  }

  </style>
  <body>


  <div id="roomNumberID" style="width: 100%; padding-top: 2%;">
  <center>
     <h2 >Add Position</h2>
     <br>

      <table  class="table">
        <thead class="thead-dark">
            <th  scope="col">Position</th>
            <th  scope="col">Salary Rate/Month</th>
            <th  scope="col">Overtime Rate/hr</th>
            <th  scope="col">Absent Rate/Day</th>
            <th  scope="col">Action</th>
        </thead>
        
          <tr>
            <form method="post">
              <td> <input type="text" name="pos" class="form-control" required></td>
              <td> <input type="text" name="sal" required></td>
              <td> <input type="text" name="ot" class="form-control" required></td>
              <td> <input type="text" name="absent" required></td>
              <td> <input type="submit" class="btn btn-success" name="savePosBTN" value="Add"></td>
            </form>
          </tr>
     </table>
     <br><br>
      
      <table class="table" style="width: 90%;">
        <thead class="thead-dark">
          <th>Position</th>
          <th>Salary</th>
          <th>Overtime Rate/hr</th>
          <th>Absent Rate/Day</th>
          <th>Action</th>
        </thead>
          <?php  foreach ($getData1 as $row) {?>
          <tr>
           <td> <?php echo $row['pos'];?> </td>
           <td> <?php echo $row['sal'];?> </td>
           <td> <?php echo $row['ot_rate'];?> </td>
           <td> <?php echo $row['absent_rate'];?> </td>
           <td> <?php echo '<a style="color:black;" href="editPos.php?id='. $row['id'].'">Update</a>';?> </td> 
          </tr> <?php } ?>
        </table>
   </div>
   <br><br><br><br><br>
  </body>
  </html>
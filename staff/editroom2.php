<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

  $type_id = 0;

   if(!empty($_GET['id'])){
        $type_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $type_id = $_POST['id'];
    }

  $result = getAllFromRoomTable($type_id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
  <head>
        <title>Edit Room Details</title>
        <script src="../scripts/jquery.js"></script>
    </head>
</head>
<body>
<style>
          .required{
              color : red;
          }

        
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}


          </style>
  <div style="width: 100%; padding-top: 2%;"> <!-- Start of div top class -->
   <center>
   <h2>Edit Room Type</h2>
   <br>
    <form method="post">

     <table class="table">
     <thead class="thead-dark">
     <tr>
                <th scope="col">ID</th>
                <th scope="col">Room Number</th>
                <th scope="col">Type</th>
                <th scope="col">Price</th>


            <?php  foreach ($result as $row) {?>
                <tr>
                 <td>  <?php echo $a = $row['id'];?> </td>
                 <td>  <?php echo $b = $row['roomNumber'];?> </td>
                 <td>  <?php echo $d = $row['roomType'];?></td>
                 <td>  <?php echo "P3000.00"; $c = 3000;?> </td>
                <?php } ?>
              </tr>
      </table>

         <?php  foreach ($result as $row) {?>
            <center> <img src="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['photos']) ; ?>" width ="300" height = "300" style = "border-style: solid; border-radius: 15px;" ></center>
    <?php } ?>
     </div>
<center>
  <div style="width: 70%;">
      <form method="POST" enctype="multipart/form-data">
                <div class="inputBox"><br> <br> <br>

                <div class="form-group row">
                <label for="roomtype" class="col-sm-4 col-form-label">Room Type<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="roomType" name="roomType" required value="<?php echo $b; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Price <span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="price" name="price" required value="<?php echo $c; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Total Number of Rooms <span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="noOfRooms" name="noOfRooms" required value="<?php echo $d; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Photo<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="file" id="image" name="image" multiple>
                </div>
                </div>
        
                    <input type="text" name="user_id" value="<?php echo $a;?>" hidden>
            
                </div>
                <center><!-- <input class="button" type="submit" name="updateRoomTypeBTN" value="Update"><input class="button" type="submit" value="Cancel"> --> <p>Still can't update data</p></center>

            </form>
</div>

</body>
</html>
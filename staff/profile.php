<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdminStaff.php");

 $getData = getAllReservations();
 
?>
<!DOCTYPE html>
<meta charset = "eng">
<html>
<head>
  
        <title>Admin's Page</title>
        <script src="../scripts/jquery.js"></script>
        <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" /> 
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" /> 
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
  <meta http-equiv="refresh" content="30">
    </head>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>

  <div style="width: 100%; padding-top:2%;"> 
   <center>
   <h2>Reservation List</h2>
   <br>
    <form method="post">
       <table class="table">
       <thead class="thead-dark">
         <th scope="col">ID</th>
         <th scope="col">Username</th>
         <th scope="col">Room Type</th>
         <th scope="col">Room Number</th>
         <th scope="col">Check-in</th>
         <th scope="col">Check-out</th>
         <th scope="col">Bill</th>
         <th scope="col">Action</th>
         </tr>
  </thead>
          <tbody>
         <?php
         foreach ($getData as $row) {?>
             <tr>
                 <td><center><?php echo $row['user_id'];?></center> </td>
                 <td> <?php echo $row['username']; ?> </td>
                 <td> <?php echo $row['room_type'];?></td>      
                 <td> <?php echo $row['room_number'];?></td>         
                 <td> <?php echo $row['check_in']; ?> </td>
                 <td> <?php echo $row['check_out']; ?> </td>
                 <td> <?php echo $row['bill']; ?> </td>
                 <td> <?php echo '<a  style="font-size: 12px; color:black;" href="approve.php?id='. $row['res_id'].'">Approve</a>';?> </td>
            </tr> <?php } ?>
          </tbody>
        </table>
        </form>
       </center><br>
    </div>
</body>
</html>
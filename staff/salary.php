<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");


 $getData = getAllAdmin();

?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
  <head>
        <title>Employee Salary</title>
        <script src="../scripts/jquery.js"></script>
        
  </head>
  <script type="text/javascript">

        function printThis(){
            window.print();
        }


    function getType(str){

          getTotal();
    }

function getTotal(id){
 // alert(id);

    //var c = this.id;
    var salary = "sal"+id;
    var absent = "absent"+id;
    var ot = "ot"+id;
    var pagibig = "pagibig"+id;
    var philhealth = "philhealth"+id;
    var sss = "sss"+id;
    var tin = "tin"+id;
    var gross = "gross"+id;
    var ded = "deduction"+id;
    var net = 'net'+id;
    var ot_rate = 'ot_rate'+id;
    var absent_rate = 'absent_rate'+id;

    var salary1 = document.getElementById(salary).value;
    var absent1 = document.getElementById(absent).value;
    var ot1 = document.getElementById(ot).value;
    var pagibig1 = document.getElementById(pagibig).value;
    var philhealth1 = document.getElementById(philhealth).value;
    var sss1 = document.getElementById(sss).value;
    var tin1 = document.getElementById(tin).value;
    var ot_rate1 = document.getElementById(ot_rate).value;
    var absent_rate1 = document.getElementById(absent_rate).value;


    var dividedSalary = (salary1/2) + (ot_rate1*ot1); //gross pay per 15 days

    //deductions
    var pagibigDed = ((salary1/100) * pagibig1); 
    var philhealthDed = (salary1/100) * philhealth1;
    var sssDed = (salary1/100) * sss1; 
    var tinDed = (salary1/100) * tin1; 
    var absentDed = absent_rate1*absent1;

    var totalDeduction = pagibigDed + philhealthDed + sssDed + tinDed + absentDed;

    var netPay = dividedSalary - totalDeduction;

    var grossPay = document.getElementById(gross).value = (dividedSalary);
    var totalDeductions = document.getElementById(ded).value = (totalDeduction);
    var totalAns = document.getElementById(net).value = (netPay);
}

  </script>
  <style type="text/css" >
     
     @media print {
        body * {
          visibility: hidden;
         }
      
      #section-to-print, #section-to-print * {

        visibility: visible;
      }
  
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
} 


  </style>
 
<body>
  <div>
    <form method="post" onsubmit="printThis()" >
      <div id="section-to-print">
      <center>
        <h2>Payroll</h2>
        <p><?php echo date("F d, Y"); ?></p>
      
      <table class="table">
        <thead class="thead-dark">
         <th  scope="col">ID</th>
         <th  scope="col">Name</th>
         <th  scope="col">Position</th>
         <th  scope="col">Absences</th>
         <th  scope="col">Overtime</th>
         <th  scope="col">Gross Pay</th>
         <th  scope="col">Total Deduction</th>
         <th  scope="col">Net Pay</th>
        </thead>
        
        <tbody>
         <?php
         foreach ($getData as $row) {
          $sal = $row["salary"];
          $pos = $row["pos"];
          $pagibig = $row["pagibigDed"];
          $philhealth = $row["philDed"];
          $sss = $row["sssDed"];
          $tin = $row["tinDed"];
          $ot_rate = $row["ot_rate"];
          $absent_rate = $row["absent_rate"];
          ?>
             <tr>
               <td><center><?php echo $idUser = $row['id'];?></center> </td>
               <td><?php echo $row['fname']." ".$row['mname']." ".$row['lname'];?></td>  
               <td><div class="input-group mb-3"><?php echo "$pos"; ?></div></td>     
               <td><input style="width: 150px;" type="text" class="form-control col-md-4"  name="absent<?php echo $idUser; ?>" id="absent<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required>
               </td>
               <td><input style="width: 150px;" type="text" class="form-control col-md-4"  name="ot<?php echo $idUser; ?>" id="ot<?php echo $idUser; ?>" value="" onkeyup="getTotal(<?php echo $idUser; ?>);" required>
               </td>

                <input type="text" id="sal<?php echo $idUser; ?>" value="<?php echo($sal); ?>" hidden>
                <input type="text" id="pagibig<?php echo $idUser; ?>" value="<?php echo($pagibig);?>" hidden>
                <input type="text" id="philhealth<?php echo $idUser; ?>" value="<?php echo($philhealth);?>" hidden>
                <input type="text" id="sss<?php echo $idUser; ?>" value="<?php echo($sss);?>" hidden>
                <input type="text" id="tin<?php echo $idUser; ?>" value="<?php echo($tin); ?>" hidden>
                <input type="text" id="ot_rate<?php echo $idUser; ?>" value="<?php echo($ot_rate); ?>" hidden>
                <input type="text" id="absent_rate<?php echo $idUser; ?>" value="<?php echo($absent_rate); ?>" hidden>
              

              <td> 
                 <div class="input-group mb-1" style="width: 200px;"><div class="input-group-prepend"><span class="input-group-text">₱</span></div> <input type="text" class="form-control col-md-5" name="gross<?php echo $idUser;?>" id="gross<?php echo $idUser;?>"  size="5px" readonly=""/>
                </div>
              </td>


              <td> 
                 <div class="input-group mb-1" style="width: 200px;"><div class="input-group-prepend"><span class="input-group-text">₱</span></div> <input type="text" class="form-control col-md-5" name="deduction<?php echo $idUser;?>" id="deduction<?php echo $idUser;?>"  size="5px" readonly=""/>
                </div>
              </td>

                <td> 
                 <div class="input-group mb-1" style="width: 200px;"><div class="input-group-prepend"><span class="input-group-text">₱</span></div> <input type="text" class="form-control col-md-5" name="net<?php echo $idUser;?>" id="net<?php echo $idUser;?>"  size="5px" readonly=""/>
                </div>
              </td>
            </tr> <?php } ?>
          </tbody>
        </table>
        </center>
      </div>
        <div style="float: right; margin-right: 1%; margin-top: 1%;">
         <button class="btn" style="background-color: #efe786;" name="saveSalaryBTN" >Preview</button>
        </div>
    </form>
  </div>  
</body>
</html>

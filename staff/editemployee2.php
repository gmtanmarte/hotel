<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

	$user_id = 0;

   if(!empty($_GET['id'])){
        $user_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $user_id = $_POST['id'];
    }

  $result = getAllFromUser($user_id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>

        <title>Edit Employee Profile</title>
        <script src="../scripts/jquery.js"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap/js/bootstrap.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>
<style>
          .required{
              color : red;
          }
          </style>
  <div style="width: 100%; padding-top: 2%;"> <!-- Start of div top class -->
   <center>
   <h2>Employee's Info</h2>
    <form method="post">
        <table class="table">
        <thead class="thead-dark">
        <tr>
         <th scope="col">ID</th>
         <th scope="col">First Name</th>
         <th scope="col">Middle Name</th>
         <th scope="col">Last Name</th>
         <th scope="col">Username</th>
         <th scope="col">Email</th>
         <th scope="col">Contact</th>
         <th scope="col">Password</th>
         
         <tbody>
             <tr>
                 <td> <?php echo $h = $result["id"];?> </td>
                 <td> <?php echo $a = $result["fname"];?> </td>
                 <td> <?php echo $b = $result["mname"];?></td>
                 <td> <?php echo $c = $result["lname"];?></td>
                 <td> <?php echo $d = $result["username"]; ?> </td>
                 <td> <?php echo $e = $result["email"]; ?> </td>
                 <td> <?php echo $f = $result["contact"]; ?> </td>
                 <td> <?php echo $g = $result["password"]; ?> </td>
            </tr>
          </tbody>
          </table>
        </form>
       </center><br>
      </table>
     </div>
<center>
  <div style="width: 70%;">
      <form method="POST">       
            <div class="form-group row">
                <label for="fname" class="col-sm-2 col-form-label">First Name<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="fname" name="fname" required value="<?php echo $a; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="mname" class="col-sm-2 col-form-label">Middle Name<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="mname" name="mname" value="<?php echo $b; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-2 col-form-label">Last Name<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $c; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-sm-2 col-form-label">Username<span class = "required">*</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="username" name="username" value="<?php echo $d; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="contact" class="col-sm-2 col-form-label">Contact number (+63)<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="number" class="form-control" id="contact" name="contact" pattern="[0-9]{10}" title="Please input valid phone number (Philippine Code)" name="contactno" required value="<?php echo $f; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." name="email"required value="<?php echo $e; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="password" minlength="6" title="Password must 6 characters and above" required>
                </div>
            </div>
  </div>
                    <input type="text" name="user_id" value="<?php echo $h;?>" hidden>
                    <center><input type="submit" class="btn btn-success" name="updateBTN" value="Update"></center>
                </div>
            </form>
</div>

</body>
</html>
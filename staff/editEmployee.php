<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

	$user_id = 0;

   if(!empty($_GET['id'])){
        $user_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $user_id = $_POST['id'];
    }

  $result = getAllFromEmployee($user_id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>

        <title>Edit Employee Profile</title>
        <script src="../scripts/jquery.js"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap/js/bootstrap.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>
<style>
          .required{
              color : red;
          }
          </style>
  <div style="width: 100%; padding-top: 2%;"> <!-- Start of div top class -->
   <center>
    <form method="post">
        <table class="table">
        <thead class="thead-dark">
          <th scope="col">ID</th>
          <th scope="col">First Name</th>
          <th scope="col">Middle Name</th>
          <th scope="col">Last Name</th>
          <th scope="col">Username</th>
          <th scope="col">Email</th>
          <th scope="col">Contact</th>
          <th scope="col">Password</th>
        </thead>
         <tbody>
             <tr>
                 <td> <?php echo $h = $result["id"];?> </td>
                 <td> <?php echo $a = $result["fname"];?> </td>
                 <td> <?php echo $b = $result["mname"];?></td>
                 <td> <?php echo $c = $result["lname"];?></td>
                 <td> <?php echo $d = $result["username"]; ?> </td>
                 <td> <?php echo $e = $result["email"]; ?> </td>
                 <td> <?php echo $f = $result["contact"]; ?> </td>
                 <td> <?php echo $g = $result["password"]; ?> </td>
            </tr>
          </tbody>
          </table>

    <table class="table" style="width: 95%;">
      <thead class="thead-dark" >
         <th scope="col">Position</th>
         <th scope="col">Pagibig</th>
         <th scope="col">Pagibig deduction(%)</th>
         <th scope="col">Philhealth</th>
         <th scope="col">Philhealth deduction(%)</th>
         <th scope="col">SSS</th>
         <th scope="col">SSS deduction(%)</th>
         <th scope="col">TIN</th>
         <th scope="col">Tax deduction(%)</th>
       </thead> 
         <tbody>
             <tr>
                 <td> <?php echo $i = $result["pos"];?> </td>
                 <td> <?php echo $j = $result["pagibig"];?> </td>
                 <td> <?php echo $k = $result["pagibigDed"];?></td>
                 <td> <?php echo $l = $result["philhealth"];?></td>
                 <td> <?php echo $m = $result["philDed"]; ?> </td>
                 <td> <?php echo $n = $result["sss"]; ?> </td>
                 <td> <?php echo $o = $result["sssDed"]; ?> </td>
                 <td> <?php echo $p = $result["tin"]; ?> </td>
                 <td> <?php echo $q = $result["tinDed"]; ?> </td>
            </tr>
          </tbody>
          </table>
        </form>
       </center><br>
      </table>
     </div>
<center>
 <form method="POST">      
  <div style="width: 40%; float: left; margin-left: 5%;">
          
            <div class="form-group row">
                <label for="fname" class="col-sm-2 col-form-label">First Name<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="fname" name="fname" required value="<?php echo $a; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="mname" class="col-sm-2 col-form-label">Middle Name</label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="mname" name="mname" value="<?php echo $b; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-2 col-form-label">Last Name<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="lname" name="lname" value="<?php echo $c; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-sm-2 col-form-label">Username<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="text" class="form-control" id="username" name="username" value="<?php echo $d; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="contact" class="col-sm-2 col-form-label">Contact number (+63)<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="number" class="form-control" id="contact" name="contact" pattern="[0-9]{10}" title="Please input valid phone number (Philippine Code)" name="contactno" required value="<?php echo $f; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Email<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." name="email"required value="<?php echo $e; ?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password<span class = "required">*</span></label>
                <div class="col-sm-10">
                <input type="password" class="form-control" id="password" name="password" minlength="6" title="Password must 6 characters and above" required>
                </div>
            </div>
  </div>

 <div style="width: 50%; float:right;">

            <div class="form-group row">
                <label for="position" class="col-sm-4 col-form-label">Position</label>
                <div  class="col-sm-5">
                <select name="position" class="form-control" required >

                      <option value selected>SELECT</option>
                      <?php 
                      include_once("../includes/query.php");
                      $getData = getPosition();
                          foreach ($getData as $row) { ?>
                          <option name = "select"> <?php echo $row['pos']; ?>
                          </option>
                      <?php } ?>
                </select> 
                </div><br> 
            </div>

            <div class="form-group row">
                <label for="pagibig" class="col-sm-4 col-form-label">Pag ibig ID</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="pagibig" minlength="12" maxlength="12" title="Input Valid Pagibig ID number" value="<?php echo($j)?>">
                </div> <br><br>
                <label for="pagibig" class="col-sm-4 col-form-label">Pag ibig Deduction</label>
                <div class="col-sm-5">
                   <input type="number" name="pagibigDed" style="width: 50%" min="0" value="<?php echo($k)?>">%
                </div>
            </div>

            <div class="form-group row">
                <label for="philhealth" class="col-sm-4 col-form-label">Philhealth ID</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="philhealth" minlength="12" maxlength="12" title="Input Valid Philhealth ID number" value="<?php echo($l)?>">
                </div><br><br>
                <label for="philhealthDed" class="col-sm-4 col-form-label">Philhealth Deduction</label>
                <div class="col-sm-5">
                   <input type="number" name="philhealthDed" style="width: 50%" min="0" value="<?php echo($m)?>">%
                </div>
            </div>

            <div class="form-group row">
                <label for="sss" class="col-sm-4 col-form-label">SSS ID<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="sss" minlength="9" maxlength="9" title="Input Valid SSS ID number" required value="<?php echo($n)?>">
                 </div><br><br>
                <label for="sss" class="col-sm-4 col-form-label">SSS Deduction<span class = "required">*</span></label>
                <div class="col-sm-5">
                   <input type="number" name="sssDed" min="0" style="width: 50%" value="<?php echo($o) ?>" required>%
                </div>
            </div>

            <div class="form-group row">
                <label for="tin" class="col-sm-4 col-form-label">TIN<span class = "required">*</span></label>
                <div class="col-sm-5">
                 <input type="text" class="form-control" minlength="9" maxlength="9" title="Input Valid TIN" name="tin"  value="<?php echo($p)?>" require >
                </div><br><br> <label for="tin" class="col-sm-4 col-form-label">TIN Deduction<span class = "required">*</span></label>
                <div class="col-sm-5">
                   <input type="number" name="tinDed" min="0" style="width: 50%" value="<?php echo($q)?>" required>%
                </div>
            </div>
  </div>

  </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div>
    <input type="text" name="user_id" value="<?php echo $h;?>" hidden>
    <center><input type="submit" class="btn btn-success" name="updateEmployeeBTN" value="Update">
    <input style="margin-left: 2%;" type="button" class="btn btn-success" onclick="window.location='superAdmin.php';" value="Cancel"></center>
</div>           
  </form>
</center>
</body>
</html>
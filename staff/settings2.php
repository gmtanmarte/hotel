<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");
session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

  $getData = getRoomType();
  $getData1 = getRoom();

 $url = $_SERVER['REQUEST_URI'];
?>

<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
        <title>Admin's Page</title>
        <script src="../scripts/jquery.js"></script>
</head>
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>

<div style="width: 100%; padding-top: 2%;">
<center>
   <h2>Settings</h2>
   <br>

          <table class="table">
          <thead class="thead-dark">
                <th  scope="col">Room Type</th>
                <th  scope="col">Room Price</th>
                <th  scope="col">Total No Of Rooms</th>
                <th  scope="col">Action</th>
                <tr>
            <?php  foreach ($getData as $row) {?>
                <tr>
                 <td> <?php echo $row['typeName'];?> </td>
                 <td> <?php echo $row['price'];?> </td>
                 <td> <?php echo $row['noOfRooms'];?></td>
                 <td> <?php echo '<a href="editRoomType.php?id='. $row['id'].'">edit</a>';?> </td> </tr> <?php } ?>
              </tr>
          </table>
      </td>
    </tr>
    <center>
    <tr>
      <td>
      <div style="width: 70%;">
              <form method="post" enctype="multipart/form-data" >
              <div class="form-group">
               <b><label><h4>Room Type</h4></label></b> <br>
             
                </div>
                <div class="form-group row">
                <label for="roomname" class="col-sm-4  col-form-label">Type Name</label>
                <div class="col-sm-5">
                <input type="text" name="roomType" class="form-control" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="roomprice" class="col-sm-4  col-form-label">Room Price (php)</label>
                <div class="col-sm-5">
                <input type="text" name="price" class="form-control" required>
                </div>
                </div>
                <div class="form-group row">
                <label for="numOfRooms" class="col-sm-4  col-form-label">Number of Rooms</label>
                <div class="col-sm-5">
                <input type="number" min="0" max="10" name="noOfRooms" class="form-control" require>
                </div>
               </div>
                <input type="file" name="image" id="image" multiple required> <br><br>
                <input type="submit" class="btn btn-success" name="saveRoomTypeBTN" value="Add"> 
            </form>
        </div>
        <br>
        <br> <br>
        <table class="table">
        <thead class="thead-dark">
                <th>Room Number</th>
                <th>Type</th>
                <th>Action</th>
                <tr>
            <?php  foreach ($getData1 as $row) {?>
                <tr>
                 <td> <?php echo $row['roomNumber'];?> </td>
                 <td> <?php echo $row['roomType'];?> </td>
                 <td> <?php echo '<a href="editRoom.php?id='. $row['id'].'">edit</a>';?> </td> </tr> <?php } ?>
              </tr>
          </table>
        <div>
        <div style="width: 70%;">
      <form method="post" enctype="multipart/form-data">
      <div class="form-group">
        <b><label><h4>Room</h4></label></b> <br>

        <div class="form-group row">
                <label for="roomname" class="col-sm-4  col-form-label">Room Number</label>
                <div class="col-sm-5">
                <input type="text" name="roomNumber" class="form-control" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="roomname" class="col-sm-4  col-form-label">Room Type</label>
                <div class="col-sm-5">
                <input type="text" name="type" class="form-control" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="roomname" class="col-sm-4  col-form-label">Room Photos</label>
                <div class="col-sm-5">
                <input type="file" name="image" id="image" multiple>
                </div>
            </div>
            <input type="submit" class="btn btn-success" name="saveRoomBTN" value="Add">
       
      </form>
    </div>

      </td>
      <td>
        

      </td>
    </tr>
  </table>
  </div>
<br><br>
    
</body>
</html>
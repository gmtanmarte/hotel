<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

	$type_id = 0;

   if(!empty($_GET['id'])){
        $type_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $type_id = $_POST['id'];
    }

  $result = getAllFromRoomType($type_id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
  <head>
        <title>Edit Room Type</title>
        <script src="../scripts/jquery.js"></script>
    </head>
</head>
<body>
<style>
          .required{
              color : red;
          }
          </style>
  <div style="width: 100%; padding-top: 2%;"> <!-- Start of div top class -->
   <center> 
   <h2>Edit Rooms</h2>
   <br>
    <form method="post">

     <table class="table">
     <thead class="thead-dark">
     <tr>
                <th scope="col">ID</th>
                <th>
                <th>
                <th scope="col">Type</th>
                <th>
                <th>
                <th scope="col">Price</th>
                <th>
                <th>
                <th scope="col">Total No Of Rooms</th>
                
            <?php  foreach ($result as $row) {?>
                <tbody>
             <tr>
               
                 <td>  <?php echo $a = $row['id'];?> </td>
                 <td></td> <td></td>
                 <td>  <?php echo $b = $row['typeName'];?></td>
                 <td></td> <td></td>
                 <td>  <?php echo $c = $row['price'];?> </td>
                 <td></td> <td></td>
                 <td>  <?php echo $d = $row['noOfRooms'];?></td>
                <?php } ?>
              </tr>
      </table>
         <?php  foreach ($result as $row) {?>
            <center> <img src="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['cover_photo']) ; ?>" width ="300" height = "300" style = "border-style: solid; border-radius: 15px;" ></center>
    <?php } ?>
     </div>
<center>
  <div style="width: 70%;">
      <form method="POST" enctype="multipart/form-data">
                <div class="inputBox"><br> <br> <br> 
                <div class="form-group row">
                <label for="roomtype" class="col-sm-4 col-form-label">Room Type<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="fname" name="fname" required value="<?php echo $b; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Price<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="price" name="price" required value="<?php echo $c; ?>">
                </div>
                </div>
                
                <div class="form-group row">
                <label for="noOfRooms" class="col-sm-4 col-form-label">Total Number of Rooms<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="noOfRooms" name="noOfRooms" required value="<?php echo $d; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="image" class="col-sm-4 col-form-label"></label>
                <div class="col-sm-4">
                <input type="file" id="image" name="image" multiple>
                </div>
                </div>


                    <input type="text" name="user_id" value="<?php echo $a;?>" hidden>
                  
                </div>
                <center><!-- <input class="button" type="submit" name="updateRoomTypeBTN" value="Update"><input class="button" type="submit" value="Cancel"> --> <p>Still can't update data</p></center>

            </form>
</div>

</body>
</html>
<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");


 $getData = getAllAdmin();
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
  <head>
        <title>Employee Salary</title>
        <script src="../scripts/jquery.js"></script>
        
  </head>
  <script type="text/javascript">

        function printThis(){
            window.print();
        }


    function getType(str){

          getTotal();
    }

    function setval(id){
      
 //var c = this.id;
    var salary = "sal"+id;
    var absent = "absent"+id;
    var ot = "ot"+id;
    var pagibig = "pagibig"+id;
    var philhealth = "philhealth"+id;
    var sss = "sss"+id;
    var tin = "tin"+id;
    var gross = "gross"+id;
    var ded = "deduction"+id;
    var net = 'net'+id;
    var ot_rate = 'ot_rate'+id;
    var absent_rate = 'absent_rate'+id;

    var salary1 = document.getElementById(salary).value;
    var absent1 = document.getElementById(absent).value;
    var ot1 = document.getElementById(ot).value;
    var pagibig1 = document.getElementById(pagibig).value;
    var philhealth1 = document.getElementById(philhealth).value;
    var sss1 = document.getElementById(sss).value;
    var tin1 = document.getElementById(tin).value;
    var ot_rate1 = document.getElementById(ot_rate).value;
    var absent_rate1 = document.getElementById(absent_rate).value;

     var dividedSalary = (salary1/2) + (ot_rate1*ot1); //gross pay per 15 days

    //deductions
    var pagibigDed = ((salary1/100) * pagibig1); 
    var philhealthDed = (salary1/100) * philhealth1;
    var sssDed = (salary1/100) * sss1; 
    var tinDed = (salary1/100) * tin1; 
    var absentDed = absent_rate1*absent1;

    var totalDeduction = pagibigDed + philhealthDed + sssDed + tinDed + absentDed;

    var netPay = dividedSalary - totalDeduction;
    document.getElementById("ab2"+id).innerHTML = "Absences: "+absent1;
    document.getElementById("ot2"+id).innerHTML = "Overtime (hrs): "+ot1;
    document.getElementById("pagibig2"+id).innerHTML = "Pagibig: ₱"+pagibigDed;
    document.getElementById("philhealth2"+id).innerHTML = "Philhealth: ₱"+philhealthDed;
    document.getElementById("sss2"+id).innerHTML = "SSS: ₱"+sssDed;
    document.getElementById("tin2"+id).innerHTML = "Tax: ₱"+tinDed;
    document.getElementById("gross2"+id).innerHTML = "Gross Pay: ₱"+dividedSalary;
    document.getElementById("ded2"+id).innerHTML = "Total Deductions: ₱"+totalDeduction;
    document.getElementById("net2"+id).innerHTML = "Net Pay: ₱"+netPay;
    document.getElementById("absentDed2"+id).innerHTML = "Absences Deduction: ₱"+absentDed;
    }

  </script>
  <style type="text/css" >
     
     @media print {
        body * {
          visibility: hidden;
         }
      
      #section-to-print, #section-to-print * {

        visibility: visible;
      }
  
      #section-to-print {
        position: absolute;
        left: 0;
        top: 0;
      }
} 


.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  margin-top: 2%;
  margin-left: 18%;
  width: 70%; /* Full width */
  height: 70%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: white; /* Black w/ opacity */
  border: solid;
  border-color: black;
  border-radius: 15px;
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  margin-bottom: 2%;
  border: 1px solid #888;
  width: 50%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

p {
    text-align: center;
    text-decoration: none;
}


  </style>
 
<body>
  <div>
    
      <div>
      <center>
        <h2>Employee Payslip</h2>
        <p><?php echo date("F d, Y"); ?></p>
      
      <table class="table">
        <thead class="thead-dark">
         <th  scope="col">ID</th>
         <th  scope="col">Name</th>
         <th  scope="col">Absences</th>
         <th  scope="col">Overtime</th>
         <th  scope="col">Action</th>
        </thead>
        
        <tbody>
         <?php
         foreach ($getData as $row) {
          $sal = $row["salary"];
          $pos = $row["pos"];
          $pagibig = $row["pagibigDed"];
          $philhealth = $row["philDed"];
          $sss = $row["sssDed"];
          $tin = $row["tinDed"];
          $ot_rate = $row["ot_rate"];
          $absent_rate = $row["absent_rate"];?>
             <tr>
               <td><center><?php echo $idUser = $row['id'];?></center> </td>
               <td><?php echo $fname = $row['fname']." ".$row['mname']." ".$row['lname'];?></td>
     
               <td> <input type="text" class="form-control col-md-4"  name="absent<?php echo $idUser; ?>" id="absent<?php echo $idUser;?>" onkeyup="setval(<?php echo $idUser; ?>);" value="0">
               </td>

               <td> 
                 <div class="input-group mb-3"><input type="text" class="form-control col-md-3" id="ot<?php echo $idUser; ?>" value="0" onkeyup="setval(<?php echo $idUser; ?>);" >
                 </div>
               </td>

                <input type="text" id="sal<?php echo $idUser; ?>" value="<?php echo($sal); ?>" hidden>
                <input type="text" id="pagibig<?php echo $idUser; ?>" value="<?php echo($pagibig);?>" hidden>
                <input type="text" id="philhealth<?php echo $idUser; ?>" value="<?php echo($philhealth);?>" hidden>
                <input type="text" id="sss<?php echo $idUser; ?>" value="<?php echo($sss);?>" hidden>
                <input type="text" id="tin<?php echo $idUser; ?>" value="<?php echo($tin); ?>" hidden>
                <input type="text" id="ot_rate<?php echo $idUser; ?>" value="<?php echo($ot_rate); ?>" hidden>
                <input type="text" id="absent_rate<?php echo $idUser; ?>" value="<?php echo($absent_rate); ?>" hidden>


              <td> 

                 <div class="input-group mb-1"><button class="btn" style="background-color: #efe786;" onclick="setval(<?php echo $row["id"] ?>); document.getElementById('modal-wrapper<?php echo $row["id"]; ?>').style.display='block';" name="slip<?php echo $idUser; ?>">Generate Payslip</button></div>

              </td>

  <div id="modal-wrapper<?php echo $row['id'];?>" class="modal">
    <span onclick="document.getElementById('modal-wrapper<?php echo $row['id'];?>').style.display='none'" class="close" title="Close PopUp">&times;</span>

<div id="section-to-print">
      <div class="modal-header" >
      <h3 style="margin-left: 43%;">Zen Hotel <br> <p style="font-size: 15px;"><?php echo date("F d, Y")?></p></h3>
      </div>
   
      <div class="modal-body"><br>
      <div class="row">
          <div class="col">Name: <?php echo $fname; ?> </div>
          <div class="col"><b>Deductions</b></div>
        </div><br>

        <div class="row">
          <div class="col">Position: <?php echo $pos; ?></div>
          <div class="col"><p id="pagibig2<?php echo $idUser; ?>">Pagibig: </p></div>
        </div><br>

        <div class="row">
          <div class="col">Monthly Salary: <?php echo $sal; ?></div>
          <div class="col"><p id="philhealth2<?php echo $idUser; ?>">Philhealth: </p></div>
        </div><br>

        <div class="row">
          <div class="col"><p id="ab2<?php echo $idUser; ?>">Absences: </p></div>
          <div class="col"><p id="sss2<?php echo $idUser; ?>">SSS: </p></div>
        </div><br>

        <div class="row">
          <div class="col"><p id="ot2<?php echo $idUser; ?>">OverTime: </p></div>
          <div class="col"><p id="tin2<?php echo $idUser; ?>">Tax: </p></div>
        </div><br>
<b>-----------------------------------------------------------------------------</b>
        <div class="row">
          <div class="col"><p id="gross2<?php echo $idUser; ?>">Gross Pay: </p></div>
          <div class="col"><p id="absentDed2<?php echo $idUser; ?>">Absences Deductions: </p></div>
        </div><br>


        <div class="row">
          <div class="col"><p id="ded2<?php echo $idUser; ?>">Total Deduction: </p></div>
          <div class="col"><p id="net2<?php echo $idUser; ?>">Net Pay: </p></div>
        </div>
</div>
<br></div>
        <center>
      <div> 
 </div>  <button type="submit" onclick="printThis()" class="btn btn-primary">Print</button><br><br></center>
              <td>
                









            <?php } ?> </tr> 
          </tbody>
        </table>
        </center>
      </div>
   
  </div>  
</body>
</html>

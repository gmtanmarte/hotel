<?php
  include("../includes/config.php");
  include("../includes/query.php"); 
  include("../includes/button_function.php");
  include("../includes/headerAdmin2.php");

  

   $url = $_SERVER['REQUEST_URI'];

    $type_id = 0;

   if(!empty($_GET['id'])){
        $type_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $type_id = $_POST['id'];
    }
    $getData = getRoomType();
    $getData1 = getAllRooms($type_id);
  ?>

  <!DOCTYPE html>
  <meta charset = "eng">
  <meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
  <html>
  <head>
          <title>Admin's Page</title>
          <script src="../scripts/jquery.js"></script>
  </head>
  <style>
      @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
  html body {
    
      margin: 0;
      padding: 0;
      overflow-x: hidden;
      font-family: 'Montserrat', sans-serif;
      font-size: 100%;
      background-color: #EEEEEE;
      color: #333333;
  }

  </style>
  <body>


  <div id="roomNumberID" style="width: 100%; padding-top: 2%;">
  <center>
     <h2 >Add Room Number</h2>
     <br>

      <table  class="table">
        <thead class="thead-dark">
            <th  scope="col">Room Number</th>
            <th  scope="col">Type</th>
            <th  scope="col">Photo</th>
            <th  scope="col">Action</th>
        </thead>
        
          <tr>
            <form method="post" enctype="multipart/form-data" >
              <td> <input type="text" name="roomNumber" class="form-control" required></td>
              <td> <?php 
                      foreach ($getData as $row) {
                        if ( $row['id'] == $type_id) {
                          echo $row["typeName"];?>
                          <input type="text" name="type_des" value="<?php echo($row["id"]);?>" hidden>
                      <?php  }
                     }?>

            </td>
            <td> <input type="file" name="image" id="image" multiple></td>
            <td> <input type="submit" class="btn btn-success" name="saveRoomBTN" value="Add"></td>
            </form>
          </tr>
     </table>
     <br><br>
      
      <table class="table" style="width: 90%;">
        <thead class="thead-dark">
          <th>Photo</th>
          <th>Room Number</th>
          <th>Action</th>
        </thead>
          <?php  foreach ($getData1 as $row) {?>
          <tr>
            <td> <img src="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['photos']) ; ?>" width ="300" height = "150" style = "border-style: solid; border-radius: 15px;" > </td>
            <td> <?php echo $row['roomNumber'];?> </td>
            <td> <?php echo '<a style="color:black;" href="editRoom.php?id='. $row['id'].'">Delete</a>';?> </td> 
          </tr> <?php } ?>
        </table>
   </div>
   <br><br><br><br><br>
  </body>
  </html>
<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");
include("../includes/headerAdmin2.php");
 
	$type_id = 0;

   if(!empty($_GET['id'])){
        $type_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $type_id = $_POST['id'];
    }

  $result = getAllFromRoomType($type_id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
  <head>
        <script src="../scripts/jquery.js"></script>
    </head>
</head>
<body>
<style>
          .required{
              color : red;
          }
          </style>
<center> 
  <div style="width: 90%; padding-top: 2%;"> <!-- Start of div top class -->
   
   <br>
    <form method="post">
     <table class="table">
     <thead class="thead-dark">
 
                <th scope="col">ID</th>
                <th>
                <th>
                <th scope="col">Type</th>
                <th>
                <th>
                <th scope="col">Price</th>
                <th>
                <th>
                <th scope="col">Total No Of Rooms</th>
                <th scope="col">Action</th>
                
            <?php  foreach ($result as $row) {?>
                <tbody>
             <tr>
               
                 <td>  <?php echo $a = $row['id'];?> </td>
                 <td></td> <td></td>
                 <td>  <?php echo $b = $row['typeName'];?></td>
                 <td></td> <td></td>
                 <td>  <?php echo $c = $row['price'];?> </td>
                 <td></td> <td></td>
                 <td><?php echo $d = $row['noOfRooms'];?></td>
                 <td>
                      <?php echo '<a style="color:black;" href="../staff/roomNumberID.php?id='.$a.'">Add room number</a>';?> |
                      <?php echo '<a href="deleteRoomType.php?id='.$a.'">Delete</a>';?> 
                 </td>
                <?php } ?>
              </tr>
      </table>
         <?php  foreach ($result as $row) {?>
            <center>  <label><b> Cover Photo</b></label> <br><img src="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['cover_photo']) ; ?>" width ="300" height = "300" style = "border-style: solid; border-radius: 15px;" ></center>
    <?php } ?>
     </div>

 <div style=" width: 80%;">
  <div style="width: 50%; float: left;">
      <form method="POST" enctype="multipart/form-data">
                <div class="inputBox"><br> <br> <br> 
                <div class="form-group row">
                <label for="roomtype" class="col-sm-4 col-form-label">Room Type<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="fname" name="roomType" required value="<?php echo $b; ?>">
                </div>
                </div>

                <div class="form-group row">
                <label for="price" class="col-sm-4 col-form-label">Price<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="price" name="price" required value="<?php echo $c; ?>">
                </div>
                </div>
                
                <div class="form-group row">
                <label for="noOfRooms" class="col-sm-4 col-form-label">Total Number of Rooms<span class = "required">*</span></label>
                <div class="col-sm-4">
                <input type="text" class="form-control" id="noOfRooms" name="noOfRooms" required value="<?php echo $d; ?>">
                </div>
                </div>
                <input type="text" name="user_id" value="<?php echo $a;?>" hidden><br>  
                </div>
                <center> <input class="btn btn-success" type="submit" name="updateRoomTypeBTN" value="Update">
                 <input style="margin-left: 2%;" type="button" class="btn btn-success" onclick="window.location='settings.php';" value="Cancel"></center>
            </form>
        </div>
  
    <div class="form-group row" style="width: 50%; float: right; margin-top: 10%;">
        <form method="POST" enctype="multipart/form-data">
        
          <label for="image"><b>Update Photo</b></label> <br>
          <input type="file" id="image" name="image" required>
          <input type="text" name="user_id" value="<?php echo $a;?>" hidden><br><br><br>
          <center> <input class="btn btn-success" type="submit" name="updateRoomPhotoBTN" value="Update"> </center>
        </form>   
      </div>
    </div>
  </center>
</body>
</html>
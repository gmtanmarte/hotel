<?php
session_start();

 $username = $_SESSION['username']; 

 if (!isset($username) == 'Admin') {
   session_destroy();
   header("location: ../forms/login.php");
 }

 $url = $_SERVER['REQUEST_URI'];
//echo $url;


?>

<!DOCTYPE html>
<html lang="">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="../images/zen.ico">
<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

</head>

<body >

<style>
.dropdown:hover>.dropdown-menu{
    display: block;
  
}
</style>

<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #efe786;">
  <a class="navbar-brand" href="../staff/superadmin.php">Welcome! Admin</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Employee
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #efe786;">
          <a class="dropdown-item" href="../forms/createEmployee.php">Create Employee</a>
          <a class="dropdown-item" href="../staff/salary.php">Payroll</a>
         <a class="dropdown-item" href="../staff/payslip.php">Payslip</a>
        </div>
      </li>
      </ul>


      <ul class="navbar-nav mr-auto"  style="margin-left: -70%;">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Settings
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #efe786;">
          <a class="dropdown-item" href="../staff/settings.php">Add Room Type</a>
           <a class="dropdown-item" href="../staff/position.php">Add Position</a>
        </div>
      </li>
      </ul>

      <form method="POST">
        <button class="btn " style="background-color: #efe786;" name="logoutBTN">Logout</button>
      </form>
  </div>
</nav>
</body>
</html>
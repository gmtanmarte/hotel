<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

$userRow = getAllFromUser($id);
$getRoomType= getRoomType(); 
$roomRows = mysqli_num_rows($getRoomType);
?>
<!DOCTYPE html>
<html lang="">
<head>
<title>Manage Your Reservation</title>
<link rel="icon" href="../images/zen.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>



<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
</head>
<body>

<style>
.dropdown:hover>.dropdown-menu{
    display: block;
}
</style>

<form method="POST">
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #efe786;" >
<div class="collapse navbar-collapse" id="navbarSupportedContent">
        </div>
      </li>
      </ul>
    </form>
    <ul class="navbar-nav mr-auto" >
      <li class="nav-item dropdown">
  <a class="navbar-brand dropdown-toggle" href="../user/profile.php" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome! <?php echo $userRow["fname"];?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #efe786;">
          <a class="dropdown-item" href="userProfile.php">My Profile</a>
          <a class="dropdown-item" href="myReservations.php">Reservations</a>
          <button class="dropdown-item" name="logoutBTN">Logout</a>
        </div>
      </li>
      </ul>
    </form>
  </div>
</nav>

</body>
</html>
   <!--- <div>
        <div class="navbar">	
          <ul>
            <li><a href="profile.php">Welcome, <?php echo $userRow["fname"];?>! </a></li>
            <li>|</li>
            <li><a href="myReservations.php" style="font-size: 16px;">My Reservation</a></li>
            <li id=logout> <form method="POST"> <button class="button" name="logoutBTN">Logout</button></form></li>
          </ul>                 
      </div>

</body>
</html>
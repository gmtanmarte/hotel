<?php
require("../includes/button_function.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }


$userRow = getAllFromUser($id);
 $getData = getAllReservations();
?>

<!DOCTYPE html>

<html lang="">
<head>
  <link rel="icon" href="../images/zen.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="../images/zen.ico">
<!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>

<script>

  function checkDates(h, m, s){


var today = new Date();
var dd = today.getDate();

var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10) {
    dd='0'+dd;
} 

if(mm<10) {

    mm='0'+mm;
} 
today = mm+'/'+dd+'/'+yyyy;


    var xhttp = new XMLHttpRequest();
    var thisTime = h+":"+m+":"+s;
//alert(today + thisTime);

    xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
        var feedback = this.responseText;
        var updatedFeedback = feedback.split(",");

        var time = updatedFeedback[0];
        var date = updatedFeedback[1]; 
        var user = updatedFeedback[2];
        //alert(feedback);
        
    }
  };
  xhttp.open("GET", "../includes/getAvailability.php?h="+ h+"&m="+m+"&s="+s, true);
  //xhttp.open("GET", "../includes/getAvailability.php?in="+h, true);
  xhttp.send();
}

</script>
</head>

<body >

<style>
.dropdown:hover>.dropdown-menu{
    display: block;
  
}
</style>

<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #efe786;">
  <a class="navbar-brand" href="../staff/profile.php">Welcome, <?php echo $userRow["username"]; ?>!</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="dropdown-item" href="../staff/avail.php">Rooms</a>
      </li>
      </ul>

      <form method="POST">
        <button class="btn " style="background-color: #efe786;" name="logoutBTN">Logout</button>
      </form> <br><br>

  </div>
</nav>
<br>
   <div>
    <h3 style="float:left; margin-left: 1%"> <p> DATE: <?php echo date("F d, Y")?></p></h3></div>
<div>
    <h3 style="float:right; margin-right: 1%;">
<script>


function startTime() {
var today=new Date();
var h=today.getHours();
var m=today.getMinutes();
var s=today.getSeconds();
var t="TIME: ";
// add a zero in front of numbers<10
h=checkTime(h);

m=checkTime(m);
s=checkTime(s);
document.getElementById('txt').innerHTML=t+h+":"+m+":"+s;

t=setTimeout(function(){startTime()},500);

checkDates(h, m, s);
}

function checkTime(i){

  if (i<10){
    
    i="0" + i;
  }

  return i;
}

</script>

<body onload="startTime()">

  <div id="txt"></div></h3> 
      </div>
</body>
</html>
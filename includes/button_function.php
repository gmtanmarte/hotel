<?php
require_once("config.php");

// Register Button -----------------------------------------------------------------------------------
 if(isset($_POST['registerBTN'])){
 require_once("query.php");
 $url = $_SERVER['REQUEST_URI'];

   $fname = trim(ucwords(strtolower($_POST['fname'])), " ");
   $mname = trim(ucwords(strtolower($_POST['mname'])), " ");
   $lname = trim(ucwords(strtolower($_POST['lname']))," "); 
   $contact = $_POST['contact'];
   $email = $_POST['email'];
   $username = $_POST['username'];
   $password = $_POST['password'];
   $cpassword = $_POST['cpassword'];
   $success = 'false';

    if ($fname == null || $lname == null) {
      
       echo '<script> alert (" Fill out all forms properly!") </script>';
    } else if ($password != $cpassword) {
       
      echo '<script> alert (" Password does not match!") </script>';
    } else {
      
      $result = checkEmailAndUsername($username, $email);
      $password = md5($password);

       if (mysqli_num_rows($result) === 0) { 

           $userRow = savingProfile($fname, $mname, $lname, $username, $contact, $email, $password);
          
           $_SESSION['id'] = $userRow["id"];
           header("location: ../user/profile.php");
          }

     echo '<script> alert (" Email or Username already exist!") </script>';
            //header("location: ../forms/registration.php");
      }
}
// End of Register button --------------------------------------------------------------------------









// Register Button -----------------------------------------------------------------------------------
 if(isset($_POST['saveEmployeeBTN'])){
 require_once("query.php");
 $url = $_SERVER['REQUEST_URI'];

   $fname = trim(ucwords(strtolower($_POST['fname'])), " ");
   $mname = trim(ucwords(strtolower($_POST['mname'])), " ");
   $lname = trim(ucwords(strtolower($_POST['lname']))," "); 
   $contact = $_POST['contact'];
   $email = $_POST['email'];
   $username = $_POST['username'];
   $password = $_POST['password'];
   $cpassword = $_POST['cpassword'];
   $position = $_POST['position'];
   $pagibig = $_POST['pagibig'];
   $pagibigDed = $_POST['pagibigDed'];
   $philhealth = $_POST['philhealth'];
   $philhealthDed = $_POST['philhealthDed'];
   $sss = $_POST['sss'];
   $sssDed = $_POST['sssDed'];
   $tin = $_POST['tin'];
   $tinDed = $_POST['tinDed'];
   $success = 'false';

    if ($fname == null || $lname == null) {
      
       echo '<script> alert (" Fill out all forms properly!") </script>';
    } else if ($password != $cpassword) {
       
      echo '<script> alert (" Password does not match!") </script>';
    } else {
      
      $result = checkEmailAndUsername($username, $email);
      $password = md5($password);

       if (mysqli_num_rows($result) === 0) { 
           
              $getData = getPosition();

              foreach ($getData as $row) {
                if ($row["pos"] == $position) {
                  $position = $row["id"];
                }
              }  

              savingEmployee($fname, $mname, $lname, $username, $contact, $email, $password, $position, $pagibig, $pagibigDed, $philhealth, $philhealthDed, $sss, $sssDed, $tin, $tinDed, $position);
           
            header("location: ../staff/superAdmin.php");
          
          } else {

            echo '<script> alert (" Email or Username already exist!") </script>';
      
          }      //header("location: ../staff/superAdmin.php");
    }
 }
// End of Register button --------------------------------------------------------------------------














































































// Login button ----------------------------------------------------------------------------------------
 if(isset($_POST['loginBTN'])){
  require_once("query.php");

   $username = trim($_POST['username']); 
   $password = trim($_POST['password']); 

   if ($password == "Password" & $username == "Admin") {
      
      $_SESSION['username'] = $username;             
      header("location: ../staff/superAdmin.php");
    }

   $password = md5($_POST['password']);

    if ($username == null || $password == null) {
      
       echo '<script> alert (" Fill out all forms properly!") </script>';
    } else {
      
      $result = checkUser($username, $password);
 
        if (mysqli_num_rows($result) == 0) {

          echo '<script> alert (" Credentials Invalid!") </script>';
        } else {
            
          foreach ($result as $row) {
              $type = $row['user_type'];
              $id = $row['user_id'];
            }

              if ($type == 'user') {
                $_SESSION['id'] = $id;
                header("location: ../user/profile.php");
              
              } else {

                $_SESSION['id'] = $id;
                header("location: ../staff/profile.php");
              }
            }
    }

 }
// End of Login button ----------------------------------------------------------------------------------------



// logout button -----------------------------------------------------------------------------------------------
 if(isset($_POST['logoutBTN'])){
   session_start();
   session_unset();
   if (session_destroy()) {
    header("location: ../forms/login.php");
    }
}
// End of logout button ---------------------------------------------------------------------------------------



// create employee button -----------------------------------------------------------------------------------------------
 if(isset($_POST['createEmployeeBTN'])){
  
  header("location: ../forms/createEmployee.php");
}
// End of create employee button ---------------------------------------------------------------------------------------



// cancel registration button -----------------------------------------------------------------------------------------------
 if(isset($_POST['cancelRegistrationBTN'])){
  
  header("location: ../staff/superAdmin.php");
}
// cancel registration button ---------------------------------------------------------------------------------------


// update employee button -----------------------------------------------------------------------------------------------
 if(isset($_POST['updateBTN'])){
  
  require_once("query.php");
   $user_id =  $_POST['user_id']; 
   $fname = trim(ucwords(strtolower($_POST['fname'])), " ");
   $mname = trim(ucwords(strtolower($_POST['mname'])), " ");
   $lname = trim(ucwords(strtolower($_POST['lname']))," "); 
   $contact = $_POST['contact'];
   $email = $_POST['email'];
   $username = $_POST['username'];
   $password = $_POST['password'];
   $password = md5($password);
   $error = 0;

    $check = checkEmailAndUsername($username, $email);

    if(mysqli_num_rows($check) == 0){

     updateEmployee($user_id, $fname, $mname, $lname, $contact, $email, $username, $password);
    } else {

      foreach ($check as $row) {
        $em = $row["email"];
        $us = $row["username"];
        $theID = $row["user_id"];

        if(($em == $email || $us == $username) && $theID === $user_id ){
        
         updateEmployee($user_id, $fname, $mname, $lname, $contact, $email, $username, $password);
         break;
        } else if (($em == $email || $us == $username) && $theID !== $user_id  ){
         
         echo '<script> alert ("username or email already exist") </script>';
         break;
        }
      }

    }
}

// update employee button -----------------------------------------------------------------------------------------------
 if(isset($_POST['updateEmployeeBTN'])){
  require_once("query.php");

   $user_id =  $_POST['user_id']; 
   $fname = trim(ucwords(strtolower($_POST['fname'])), " ");
   $mname = trim(ucwords(strtolower($_POST['mname'])), " ");
   $lname = trim(ucwords(strtolower($_POST['lname']))," "); 
   $contact = $_POST['contact'];
   $email = $_POST['email'];
   $username = $_POST['username'];
   $password = $_POST['password'];
   $position = $_POST['position'];
   $pagibig = $_POST['pagibig'];
   $pagibigDed = $_POST['pagibigDed'];
   $philhealth = $_POST['philhealth'];
   $philhealthDed = $_POST['philhealthDed'];
   $sss = $_POST['sss'];
   $sssDed = $_POST['sssDed'];
   $tin = $_POST['tin'];
   $tinDed = $_POST['tinDed'];

    $check = checkEmailAndUsername($username, $email);

    if(mysqli_num_rows($check) == 0){

     updateEmployeeNajud($fname, $mname, $lname, $username, $contact, $email, $password, $position, $pagibig, $pagibigDed, $philhealth, $philhealthDed, $sss, $sssDed, $tin, $tinDed, $position, $user_id);

    } else {

      foreach ($check as $row) {
        $em = $row["email"];
        $us = $row["username"];
        $theID = $row["user_id"];

        if(($em == $email || $us == $username) && $theID === $user_id ){
        
          updateEmployeeNajud($fname, $mname, $lname, $username, $contact, $email, $password, $position, $pagibig, $pagibigDed, $philhealth, $philhealthDed, $sss, $sssDed, $tin, $tinDed, $position, $user_id);
         break;

        } else if (($em == $email || $us == $username) && $theID !== $user_id  ){
         
         echo '<script> alert ("username or email already exist") </script>';
         break;
        }
      }

    }
}
































// End of update employee button ---------------------------------------------------------------------------------------



// save room type button -----------------------------------------------------------------------------------------------
 if(isset($_POST['saveRoomTypeBTN'])){
 require_once("query.php");
   
   $roomType = trim(ucwords(strtolower($_POST['roomType'])), " ");
   $price = $_POST['price'];
   $noOfRooms = $_POST['noOfRooms'];
   $filename = $_FILES['image']['tmp_name'];
   $destination = "../images/upload/" . $_FILES['image']['name']; //filepath sa image
   $image_file= addslashes(file_get_contents($_FILES['image']['tmp_name'])); // store sa database
   $error = 0;
   $results = checkDuplicateRoomType($roomType);

  if (mysqli_num_rows($results) == 0) {

    roomType($roomType, $price, $noOfRooms, $filename, $destination, $image_file);

   } else {

   } 

  // header("location: ../staff/settings.php");
}

 if(isset($_POST['updateRoomTypeBTN'])){
 require_once("query.php");

   $user_id = $_POST['user_id'];
   $roomType = trim(ucwords(strtolower($_POST['roomType'])), " ");
   $price = $_POST['price'];
   $noOfRooms = $_POST['noOfRooms'];

    updateRoomType($user_id, $roomType, $price, $noOfRooms);
}


 if(isset($_POST['updatePosBTN'])){
 require_once("query.php");

   $user_id = $_POST['user_id'];
   $pos = trim(ucwords(strtolower($_POST['pos'])), " ");
   $sal = $_POST['sal'];
   $ot = $_POST['ot'];
   $absent = $_POST['absent'];

    updatePosition($user_id, $pos, $sal, $ot, $absent);
}


if(isset($_POST['updateRoomPhotoBTN'])){
 require_once("query.php");

   $user_id = $_POST['user_id'];
   $filename = $_FILES['image']['tmp_name'];
   $destination = "../images/upload/" . $_FILES['image']['name']; //filepath sa image
   $image_file= addslashes(file_get_contents($_FILES['image']['tmp_name'])); // store sa database

    updateRoomTypes($user_id, $filename, $destination, $image_file);
}


// save room type button ---------------------------------------------------------------------------------------



// save room button -----------------------------------------------------------------------------------------------
 if(isset($_POST['saveRoomBTN'])){
 require_once("query.php");
   
   $roomNumber = trim(ucwords(strtolower($_POST['roomNumber'])), " ");
   $type = $_POST['type_des'];
 
   $filename = $_FILES['image']['tmp_name'];
   $destination = "../images/upload/" . $_FILES['image']['name']; //filepath sa image
   $image_file= addslashes(file_get_contents($_FILES['image']['tmp_name'])); // store sa database

   $results = getAllFromRoom($roomNumber);

  if (mysqli_num_rows($results) == 0) {

    room($roomNumber, $type, $filename, $destination, $image_file);

   } else {

   } 
}

if(isset($_POST['updateRoomBTN'])){
 require_once("query.php");

   $user_id = $_POST['user_id'];
   $filename = $_FILES['image']['tmp_name'];
   $destination = "../images/upload/" . $_FILES['image']['name']; //filepath sa image
   $image_file= addslashes(file_get_contents($_FILES['image']['tmp_name'])); // store sa database

    updateRoomPhoto($user_id, $filename, $destination, $image_file);
}

// save room type button ---------------------------------------------------------------------------------------



// diff destination


// ------------------------------------------STAFF -----------------------------------------
if(isset($_POST['welcomeBTN'])){

 $url = $_SERVER['REQUEST_URI'];

  if ($url == "/hotel/staff/superAdmin.php" || $url == "/hotel/staff/settings.php" || $url == "/hotel/staff/generateSalary.php" || $url == "/hotel/staff/editEmployee.php") {  

      header("location: superAdmin.php");
    } else {

      header("location: ../staff/superAdmin.php");
    }
}

if(isset($_POST['setBTN'])){

 $url = $_SERVER['REQUEST_URI'];

  if ($url == "/hotel/staff/superAdmin.php" || $url == "/hotel/staff/settings.php" || $url == "/hotel/staff/generateSalary.php" || $url == "/hotel/staff/editEmployee.php"){  

      header("location: settings.php");
    } else {

      header("location: ../staff/settings.php");
    }
}


// ------------------------------------------FORMS -----------------------------------------
if(isset($_POST['createBTN'])){

 $url = $_SERVER['REQUEST_URI'];

  if ($url == "/hotel/forms/createEmployee.php") {  

     header("location: createEmployee.php");
    } else {
        header("location: ../forms/createEmployee.php");
      
    }
}


// end diff destination

 if(isset($_POST['saveSalaryBTN'])){
 require_once("query.php");

 $getData = getAllAdmin();
 $checkDuplicate = checkSalaryTable();

     if (mysqli_num_rows($checkDuplicate) == 0) {

     foreach ($getData as $row) {
          
          $employeeID = $row['id'];
          $ot = $_POST["ot".$employeeID];
          $absent = $_POST["absent".$employeeID];
          $gross = $_POST["gross".$employeeID];
          $deduction = $_POST["deduction".$employeeID];
          $net = $_POST["net".$employeeID];

          idToSalary($employeeID, $ot, $absent, $gross, $deduction, $net);
      }
    }
  
  header("location: ../staff/salary.php");
}


if(isset($_POST['reserveBTN'])){
  require_once("query.php");
  
  
  $user_id = $_POST["userID"]; 
  $room_id = $_POST["roomID"]; 
  $checkIn = $_POST["checkIn"]; 
  $checkOut = $_POST["checkOut"];
 // echo "$checkIn.$checkOut";
 
  $dateNow = date("Y-m-d");
  $date1=date_create($checkIn);
  $date2=date_create($checkOut);
  $forBillingIn = date_format($date1,"Y-m-d");
  $forBillingOut = date_format($date2,"Y-m-d");
  //echo "$forBillingIn.$forBillingOut";

  $inCheck = substr($forBillingIn, 0,4).substr($forBillingIn, 5,2).substr($forBillingIn, 8);
  $outCheck = substr($forBillingOut, 0,4).substr($forBillingOut, 5,2).substr($forBillingOut, 8);
  $nowDate = substr($dateNow, 0,4).substr($dateNow, 5,2).substr($dateNow, 8);

  $getPrice = getRoomPrice($room_id);

      foreach ($getPrice as $row) {
        $perDay = $row["price"];
      }

      $bill = ($outCheck - $inCheck) * $perDay;
          //echo "<br>$perDay<br>";
         // echo "$outCheck<br>$inCheck<br>$nowDate<br>Bill: $bill";
      
   if ($inCheck<$nowDate) {
       
     echo '<script> alert ("Invalid Input! You Cant reserve beyond todays date!") </script>';

   } else if ($inCheck>$outCheck) {

     echo '<script> alert ("Invalid Input! Please check the dates you input") </script>';
   } else {

    $reservationResult = checkAvailableRoom($checkIn, $checkOut, $room_id);

    if (mysqli_num_rows($reservationResult) == 0) {
      
      reserveRoom($checkIn, $checkOut, $room_id, $user_id, $bill);
      $userRow = getAllFromUser($user_id);
      $num = "0".$userRow["contact"];
      $number= $num;
      $message = substr($userRow["fname"], 0,3) .", Your reservation will expire 6hrs prior to your check in date (".$checkIn.")Bill: $bill php";
      $fullMessage = $message;
      
      $apicode = "TR-HYACI161931_AGD97";

      $result = itexmo($number,$message,$apicode);

        if ($result == ""){
          echo "iTexMo: No response from server!!!
          Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.  
          Please CONTACT US for help. ";  

      }else if ($result == 0){
        
        echo '<script> alert ("You have successfuly reserved this room. A message will be sent to you shortly") </script>';
     } else { 

      echo '<script> alert ("Error") </script>';
      //echo "Error Num ". $result . " was encountered!";
    }
     
    } else {
        
      echo '<script> alert ("Room Not Available") </script>';
    }
   }
 }

// cancel reservation

 if(isset($_POST['cancelReservationBTN'])){
  $res_id = $_POST["res_id"]; 
 
  $delete = "DELETE FROM reservation WHERE reservation_id = $res_id";
  mysqli_query($link,$delete);
 }

// update
 if(isset($_POST['updateReservationBTN'])){

  $res_id = $_POST["res_id"]; 
 
  $delete = "DELETE FROM reservation WHERE reservation_id = $res_id";
  mysqli_query($link,$delete);

  header("location: ../user/profile.php");

 }






/*if(isset($_POST['reserveBTN'])){
  require_once("query.php");
  
  $user_id = $_POST["userID"]; 
  $room_id = $_POST["roomID"]; 
  $checkIn = $_POST["checkIn"];
  $checkOut = $_POST["checkOut"];
  $dateNow = date("Y-m-d");
  $inCheck = substr($checkIn, 0,4).substr($checkIn, 5,2).substr($checkIn, 8);
  $outCheck = substr($checkOut, 0,4).substr($checkOut, 5,2).substr($checkOut, 8);
  $nowDate = substr($dateNow, 0,4).substr($dateNow, 5,2).substr($dateNow, 8);

  $getPrice = getRoomPrice($room_id);

      foreach ($getPrice as $row) {
        $perDay = $row["price"];
      }
      $bill = ($outCheck - $inCheck) * $perDay;
      
   if ($inCheck<$nowDate) {
       
     echo '<script> alert ("Invalid Input! You Cant reserve beyond todays date!") </script>';

   } else if ($inCheck>$outCheck) {

     echo '<script> alert ("Invalid Input! Please check the dates you input") </script>';
   } else {

    $reservationResult = checkAvailableRoom($checkIn, $checkOut, $room_id);

    if (mysqli_num_rows($reservationResult) == 0) {
      
      reserveRoom($checkIn, $checkOut, $room_id, $user_id, $bill);
      $userRow = getAllFromUser($user_id);
      $num = "0".$userRow["contact"];
      $number= $num;
      $message = substr($userRow["fname"], 0,3) .", Your reservation will expire 6hrs prior to your check in date (".$checkIn.")Bill: $bill php";
      $fullMessage = $message;
      $apicode = "TR-HYACI161931_AGD97";

      $result = itexmo($number,$message,$apicode);

        if ($result == ""){
          echo "iTexMo: No response from server!!!
          Please check the METHOD used (CURL or CURL-LESS). If you are using CURL then try CURL-LESS and vice versa.  
          Please CONTACT US for help. ";  

      }else if ($result == 0){
        
        echo '<script> alert ("You have successfuly reserved this room. A message will be sent to you shortly") </script>';
     } else { 

      echo '<script> alert ("Error") </script>';
      //echo "Error Num ". $result . " was encountered!";
    }
     
    } else {
        
      echo '<script> alert ("Room Not Available") </script>';
    }
   }
 }*/


// save room type button -----------------------------------------------------------------------------------------------
 if(isset($_POST['savePosBTN'])){
 require_once("query.php");
   
   $pos = trim(ucwords(strtolower($_POST['pos'])), " ");
   $sal = $_POST['sal'];
   $ot = $_POST['ot'];
   $absent = $_POST['absent'];
   
   $results = checkDuplicatePos($pos);

  if (mysqli_num_rows($results) == 0) {

    insertPos($pos, $ot, $absent, $sal);

   } else {

   } 

  // header("location: ../staff/settings.php");
}



// save room type button -----------------------------------------------------------------------------------------------
 if(isset($_POST['goBTN'])){
 require_once("query.php");
   
   $searchEmployee = $_POST['searchEmployee'];
    


}
























































































?>
<?php

require("../includes/button_function.php");
?>

<!DOCTYPE html>
<html lang="">
<head>
<title>Admin</title>
<link rel="icon" href="../images/zen.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="../css/style1.css" />	
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
</head>

<body >
    <div>
        <nav class="navbar">	
          <ul>
            <li><form method="POST"><button class="button" name="welcomeBTN" style="font-size: 20px;">Welcome, Admin!</button></li>
            <li>|</li>
            <li><button class="button" name="createBTN">Create Employee</button></li>
            <li>|</li>
            <li><button class="button" name="setBTN">Settings</button></li>
            <li id=logout><button class="button" name="logoutBTN">Logout</button></form></li>
          </ul>                 
      </nav>

</body>
</html>
 <script type="text/javascript">

      function forms(){

        UIkit.notification("<span uk-icon='icon: warning'> </span>  Fill out all forms properly.", {status: 'danger'} );
      }
      
     function passwordInvalid(){

        UIkit.notification("<span uk-icon='icon: warning'> </span>  Password input invalid.", {status: 'danger'} );
      }

     function recognize(){

        UIkit.notification("<span uk-icon='icon: warning'> </span> Email already exist.", {status: 'danger'} );
      } 

     function message(){

        UIkit.notification("<span uk-icon='icon: warning'> </span>  No Photo Selected!", {status: 'warning'} );
      }

     function fieldEmpty(){

         UIkit.notification("<span uk-icon='icon: warning'> </span>  Input Your Desired Password!", {status: 'warning'} );
      }

     function successMsg(){

         UIkit.notification("<span uk-icon='icon: check'> </span>  Password Successfuly Changed.", {status: 'success'} );
      }

    function remarks(){

        UIkit.notification("<span uk-icon='icon: check'> </span>  Remarks Saved!", {status: 'success'} );
      
      }

    function profileSaved(){

        UIkit.notification("<span uk-icon='icon: warning'> </span>  Profile Saved!", {status: 'success'} );
      }
    </script>
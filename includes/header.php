<?php
include("../includes/config.php");
include("../includes/query.php"); 
include("../includes/button_function.php");

session_start();
 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

$userRow = getAllFromUser($id);
$getRoomType= getRoomType(); 
$roomRows = mysqli_num_rows($getRoomType);
?>

<!DOCTYPE html>

<html lang="">
<head>
<title>Manage your reservation</title>
<link rel="icon" href="../images/zen.ico">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link href="../css/layout.css" rel="stylesheet" type="text/css" media="all">
<link rel="stylesheet" type="text/css" href="../css/style1.css" />	
<script src="layout/scripts/jquery.min.js"></script>
<script src="layout/scripts/jquery.backtotop.js"></script>
<script src="layout/scripts/jquery.mobilemenu.js"></script>
</head>
<body>
    <div>
        <div class="navbar">	
          <ul>
            <li><a href="profile.php">Welcome, <?php echo $userRow["fname"];?>! </a></li>
            <li>|</li>
            <li><a href="myReservations.php" style="font-size: 16px;">My Reservation</a></li>
            <li id=logout> <form method="POST"> <button class="button" name="logoutBTN">Logout</button></form></li>
          </ul>                 
      </div>

</body>
</html>
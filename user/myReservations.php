<?php 
include("../includes/header2.php");

echo $id;
$res = getMyReservations($id);
?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
        <title>Rooms</title>
        <script src="../scripts/jquery.js"></script>
     
                <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
    
    function editReservation(res_id) {
        alert(res_id);

    }


</script>
</head> 
<style>

@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}
.underlay {
    position: relative;
    z-index: 1;
    width: 100%;
    height: 54px;
}

.logsContain {
    padding: 0 15em;
    margin-top: 5vw;
}

.logItem {
    width: 100%;
    margin-bottom: .15em;
    padding: 1rem;
    border-left: .3em solid rgb(236, 212, 104);
    background: rgba(236, 212, 104, .15);
}

.logItem h4 {
    margin-top: 0;
}

.logItem p {
    margin: 0;
}

.logItem:hover {
  opacity: 0.5;
  filter: alpha(opacity=50); /* For IE8 and earlier */
}

.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 10px;
  border: 1px solid #888;
  width: 50%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
<body>

        <div class = "underlay"></div>

        <div class="logsContain">

            <?php if (mysqli_num_rows($res) == 0) { ?>
            
                    <div class = "logItem">
                        <p style="text-align: center;">You have no history.</p>
                    </div>

           <?php } else {

                foreach ($res as $row) { 

                  $in = date_create($row["check_in"]);
                  $out = date_create($row["check_out"]);
                  $dateNow = date("Y-m-d");

                  $inCheck = substr($row["check_in"], 0,4).substr($row["check_in"], 5,2).substr($row["check_in"], 8);
                  $outCheck = substr($row["check_out"], 0,4).substr($row["check_out"], 5,2).substr($row["check_out"], 8);
                  $nowDate = substr($dateNow, 0,4).substr($dateNow, 5,2).substr($dateNow, 8);

                  $bill = ($outCheck - $inCheck) * $row["room_price"];

                  if ($row["check_in"] >= $dateNow ) { ?>
                   
                    <div class = "logItem" onclick="editReservation(<?php echo $row["res_id"] ?>); document.getElementById('modal-wrapper<?php echo $row["res_id"]; ?>').style.display='block';">
                      <h4> <?php echo date_format($in, "F d, Y ") ." - ". date_format($out, "F d, Y ")  ?></h4>
                      <p> <i><?php echo $row["fname"] ." reserved room ". $row["room_number"]. " of type ". $row["room_type"]; ?></i> </p>
                      <p> <i><?php echo "Total Bill: ₱". $bill .".00"; ?></i> </p>
                    </div> <br>


  <div id="modal-wrapper<?php echo $row['res_id'];?>" class="modal">
    <form method="post" class="modal-content" >
    <span onclick="document.getElementById('modal-wrapper<?php echo $row['res_id'];?>').style.display='none'" class="close" title="Close PopUp">&times;</span>

      <div class="modal-header">
        <input type="text" name="res_id" value="<?php echo($row["res_id"])?>" hidden>
        <button type="submit" name="updateReservationBTN" class="btn btn-primary">Edit Reservation</button>
        <button type="submit" name="cancelReservationBTN" class="btn btn-primary">Cancel Reservation</button>
      </div>
    </form>
 </div>                  











                <?php }   
                } 


                foreach ($res as $row) {
                    
                $in = date_create($row["check_in"]);
                $out = date_create($row["check_out"]);
                $dateNow = date("Y-m-d");

                  if ($row["check_in"] < $dateNow ) { ?>

                    <div class = "logItem">
                      <h6>Reservation History</h6>
                      <h4> <?php echo date_format($in, "F d, Y ") ." - ". date_format($out, "F d, Y ")  ?></h4>
                      <p> <i><?php echo $row["fname"] ." reserved room ". $row["room_number"]. " of type ". $row["room_type"]; ?></i> </p>
                    </div> <br>

               <?php }
            }
        } ?>
               

        
        </div>
    </body>
</html>

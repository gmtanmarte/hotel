<?php
include("../includes/header2.php");
?>  
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
  
        <title>Manage Your Reservation</title>
        <script src="../scripts/jquery.js"></script>
                <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
  
</head> 
<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}
</style>
<body>
  <div style="width: 100%;" class="container bootstrap snippet">   
     
  <div class="row">
      
        </div><!--/col-3-->
    	<div class="col-sm-9">
      <div class="tab-content">
            <div class="tab-pane active" id="home">
                <hr>
                  <form class="form" method="post" id="registrationForm">

                       <div class="form-group">
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Username</h4></label>
                              <input type="text" class="form-control" name="username" id="last_name" value="<?php echo($userRow["username"]);?>" required>
                          </div>
                      </div>

                      <div class="form-group">
                          <input type="text" name="user_id" value="<?php echo($userRow["id"]); ?>" hidden>
                          <div class="col-xs-6">
                              <label for="first_name"><h4>First name</h4></label>
                              <input type="text" class="form-control" name="fname" id="first_name" placeholder="first name" value="<?php echo($userRow["fname"]);?>">
                          </div>
                      </div>

                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Middle name</h4></label>
                              <input type="text" class="form-control" name="mname" id="last_name" placeholder="last name" value="<?php echo($userRow["mname"]);?>" required>
                          </div>
                      </div>

                      <div class="form-group">
                          
                          <div class="col-xs-6">
                            <label for="last_name"><h4>Last name</h4></label>
                              <input type="text" class="form-control" name="lname" id="last_name" placeholder="last name" value="<?php echo($userRow["lname"]);?>" required>
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="phone"><h4>Contact number (+63)</h4></label>
                              <input type="text" class="form-control" name="contact" id="contact" pattern="[0-9]{10}" title="Please input valid phone number (Philippine Code)" value="<?php echo($userRow["contact"]);?>" required>
                          </div>
                      </div>
          
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Email</h4></label>
                              <input type="email" class="form-control" name="email" id="email" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." value="<?php echo($userRow["email"]);?>" required>
                          </div>
                      </div>

                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="password"><h4>Password</h4></label>
                              <input type="password" class="form-control" name="password" minlength="6" title="Password must 6 characters and above" required>
                          </div>
                      </div>

                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" name="updateBTN" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button> 
                            </div>
                      </div>
              	</form>
              <hr>
</body>
</html>
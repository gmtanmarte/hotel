<?php
include("../includes/header2.php");
?>  
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
  
        <title>Manage Your Reservation</title>
        <script src="../scripts/jquery.js"></script>
                <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
  
</head> 
<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

p {
    color: white;
    margin-top: 20%;
    padding: 10px 10px 10px 10px;
    text-align: center;
    text-decoration: none;
}
td:hover {
  opacity: 0.5;
  filter: alpha(opacity=50); /* For IE8 and earlier */
}
#text-block{

    color: white;
    border-width:2px;     
}



</style>
<body>
  <div style="width: 100%; ">   
      <?php 
      
        if ($roomRows == 0) { ?>

          <center><b><h2><i class="button">Hotel is not yet available for booking</i></h2></b> </center>

        <?php } else { ?>

          <table style="border-style: none; outline: none;" class="table">
          <?php $iterate = 1; 

            while($roomRows>=$iterate) { ?><tr>

             <?php  
                foreach ($getRoomType as $row){?>

                  <td background="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['cover_photo']) ; ?>" style="height:600px; width: 400px; border-style: none; background-position: center; background-repeat: no-repeat; background-size:1100px 700px;"><center><div>

                    <?php  
                          echo '
                          <center><div class="badge badge-dark text-wrap" style=" width: 9rem;margin-top:20%; "> <a id="text-block" style="margin-top; text-decoration: none;" href="room.php?id='. $row['id'].'"> <p><h6>
                            '.$row['typeName'].'</p></a></div></div></h6>
                           </div>';
                          $iterate++;
                    ?>
                      
                  </div></center></td>

              <?php if (($iterate%2) == 0) { continue; } ?>
                </tr> 

          <?php  }
        }?></table>
      <?php  } // end for else ?>

  </div>

</body>
</html>
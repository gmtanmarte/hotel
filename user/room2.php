<?php
include("../includes/header2.php");

 $result = getAllFromUser($id);
 $resultRooms = getRoomType();
 $noOfResult = mysqli_num_rows($resultRooms); 
 
 $room_id = 0;

 if(!empty($_GET['id'])){
        $room_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $room_id = $_POST['id'];
    }

 $getRooms = getAllRooms($room_id);
 $rowsForGetRooms = mysqli_num_rows($getRooms);

?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>
        <title>Rooms</title>
        <script src="../scripts/jquery.js"></script>
                <!-- bootstrap -->
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../bootstrap/js/bootstrap.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
  
</head> 
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
<body>
<center>
  <div style="width: 100%; padding-top: 2%;">   

      <?php 
        if ($rowsForGetRooms == 0) { ?>

          <center><b><h2><i class="button">No rooms yet</i></h2></b> </center>

        <?php } else { ?>

          <table style="border-style: none;" outline: none;" class="table">
          <?php $iterate = 1; 

            while($rowsForGetRooms>=$iterate) { ?><tr class="divForProfile">

             <?php  

                foreach ($getRooms as $row){?>

                  <td background="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['photos']) ; ?>" style="height: 300px; width: 300px; border-style: solid; background-position: center; background-repeat: no-repeat; background-size: cover;"><center><div>

                    <?php  
                          echo '<a style="margin-top: 20%; border-style: solid;" href="book.php?id='. $row['id'].'"> <p style="padding: 5px 5px 5px 5px;"> '.$row['roomNumber'].'</p> </a>';
                          $iterate++;
                    ?><br>

                  </div></center></td>

              <?php if (($iterate%2) == 0) { continue; } ?>
                </tr> 

          <?php  }
        }?></table>
      <?php  } // end for else ?>

  </div>

</body>
</html>
<?php
include("../includes/header2.php");

 $result = getAllFromUser($id);
 $resultRooms = getRoomType();
 $noOfResult = mysqli_num_rows($resultRooms); 
 
 $room_id = 0;

 if(!empty($_GET['id'])){
        $room_id = $_REQUEST['id'];
    }

    if (!empty($_POST['id'] )){
        $room_id = $_POST['id'];
    }

 $getRooms = getAllRooms($room_id);
 $rowsForGetRooms = mysqli_num_rows($getRooms);

?>
<!DOCTYPE html>
<meta charset = "eng">
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
<html>
<head>

        <title>Rooms</title>

        <script src="../scripts/jquery.js"></script>
        <link rel="stylesheet"  type="text/css" href="../css/userReservations.css">
                <!-- bootstrap -->
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css" />	
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />	
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="../bootstrap/js/bootstrap.js"></script>
        <script src="../bootstrap/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
         <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>
        <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />





  
</head> 
<style>
@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 10px;
  border: 1px solid #888;
  width: 50%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

p {
    margin-top: 20%;
    padding: 10px 10px 10px 10px;
    text-align: center;
    text-decoration: none;
}
button:hover {
  opacity: 0.5;
  filter: alpha(opacity=50); /* For IE8 and earlier */
}
        .error{
    color:red;
  }

</style>

<script type="text/javascript">
    
$("input.date").datepicker({
    minDate: 0  
});
   function setval(vol){

    document.cookie = "vol = " + vol;
  }

  function checkDates(id){

    var a = "in"+id;
    var b= "out"+id;

    var c = document.getElementById(a).value;
    var d = document.getElementById(b).value;

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
        var feedback = this.responseText;
        var updatedFeedback = feedback.split(",");

        alert(updatedFeedback[0]);
        var bill = updatedFeedback[1]; 
       // document.getElementById('bill'+id).value = bill;
    }


  };

  xhttp.open("GET", "../includes/getAvailability.php?in="+ c+"&out="+d+"&roomID="+id, true);
  xhttp.send();
}

</script>
<body>
<center>
  <div style="width: 100%;">   

      <?php 
        if ($rowsForGetRooms == 0) { ?>

          <center><b><h2><i class="button">No rooms yet</i></h2></b> </center>

        <?php } else { ?>

          <table style="border-style: none; outline: none;" class="table">
          <?php $iterate = 1; 

            while($rowsForGetRooms>=$iterate) { ?><tr>

             <?php  

                foreach ($getRooms as $row){?>
                  <td background="data:image/jpg/png; base64, <?php echo $p = base64_encode($row['photos']) ;?>"style="height:600px; width: 400px; border-style: none; background-position: center; background-repeat: no-repeat; background-size:1100px 700px;">
                  <center><div>
                     
                    <button style=" width: 10rem; margin-top:12%" onclick="setval(<?php echo $row["id"] ?>); document.getElementById('modal-wrapper<?php echo $row["id"]; ?>').style.display='block';" class="badge badge-dark text-wrap"> <?php echo $row["roomNumber"]; ?> </button>
                       <?php $iterate++;?>
                    <br>
                  </div></center>


  <div id="modal-wrapper<?php echo $row['id'];?>" class="modal">
    <form method="post" class="modal-content" >
    <span onclick="document.getElementById('modal-wrapper<?php echo $row['id'];?>').style.display='none'" class="close" title="Close PopUp">&times;</span>

      <div class="modal-header">
      <h3 style="align-self: center;"> &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Reserve Room <?php echo $row["roomNumber"];?></h3>
      </div>

      <div class="modal-body"><br>
      <div class="row">
          <div class="col">Date: <?php echo date("F d, Y"); ?></div>
          <div class="col">Number: 0<?php echo $userRow["contact"]?></div>
        </div><br>

        <div class="row">
          <div class="col">Name: <?php echo $userRow["fname"]." ".substr($userRow["mname"], 0,1).". ".$userRow["lname"]; ?></div>
          <div class="col"></div>
        </div><br>

        <div class="row">
          <div class="col">Check In Date<input name="checkIn" id="<?php echo $row['id']; ?>" class="form-control" required></div>
         
          <div class="col">Check Out Date<input name="checkOut" id="<?php echo $row['id']; ?>" class="form-control1" required></div>
      
        </div><br>

        <input type="text" name="roomID" value="<?php echo $row['id']; ?>" hidden>
        <input type="text" name="userID" value="<?php echo $userRow['id']; ?>" hidden>
        <center><button type="submit" name="reserveBTN" class="btn btn-primary">Submit</button></center>
      <div>
    </form>
 </div>                  

  </td>
<?php if (($iterate%2) == 0) { continue; } ?>
                </tr> 
          <?php  }
        }?></table>
      <?php  } // end for else ?>

      <script>

        $('.form-control').datetimepicker({ 
          dateFormat: "mm-dd-yy", 
          footer: true, modal: true , uiLibrary: 'bootstrap4', 
         iconsLibrary: 'fontawesome', 
           
        });

         $('.form-control1').datepicker({ 
          footer: true, modal: true , uiLibrary: 'bootstrap4',
          iconsLibrary: 'fontawesome',
        });
       
       </script>
       
</body>
</html>

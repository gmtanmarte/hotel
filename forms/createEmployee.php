<?php 
include("../includes/headerAdmin2.php");
include("../includes/button_function.php");
?>

<!DOCTYPE html>
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0" charset = "utf-8">
<html>
    <head>
        <title>Register for an account</title>
        <link rel="icon" href="../images/zen.ico">

    </head>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
    <body>
        <div id = "darkOverlay"></div>
        <div class="loginBox">
          <style>
          .required{
              color : red;
          } 
          </style>
        </div>
 <br><br>
    
 <center><h3>Register Employee</h3> <br>

  <form method="POST">      
     <div style="width: 50%; float: left;">

            <div class="form-group row">
                <label for="fname" class="col-sm-4 col-form-label">First Name<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="fname" required value="<?php if (isset($fname)) {if($success != 'true'){echo $fname;}}?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="mname" class="col-sm-4 col-form-label">Middle Name</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="mname" id="" value="<?php if (isset($mname)) {if($success != 'true'){echo $mname;}}?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="lname" class="col-sm-4 col-form-label">Last Name<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="lname" id="" required value="<?php if (isset($lname)) {if($success != 'true'){echo $lname;}}?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="username" class="col-sm-4 col-form-label">Username<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" id="username" name="username" require>
                </div>
            </div>

            <div class="form-group row">
                <label for="email" class="col-sm-4 col-form-label">Email<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." name="email"required>
                </div>
            </div>

            <div class="form-group row">
                <label for="contact" class="col-sm-4 col-form-label">Contact number (+63)<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" id="contact" name="contact" pattern="[0-9]{10}" maxlength="10" title="Please input valid phone number (Philippine Code)" name="contactno" required value="<?php if (isset($contact)) {if($success != 'true'){echo $contact;}}?>"> 
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-4 col-form-label">Password<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="password" class="form-control" id="password" name="password" minlength="6" title="Password must 6 characters and above" required >
                </div>
            </div>

            <div class="form-group row">
                <label for="password" class="col-sm-4 col-form-label">Confirm Password<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="password" class="form-control" name="cpassword" id="" title="Password must 6 characters and above" required >
                </div>
            </div>

  </div>
  <div style="width: 50%; float:right;">

            <div class="form-group row">
                <label for="position" class="col-sm-4 col-form-label">Position</label>
                <div  class="col-sm-5">
                <select name="position" class="form-control" required >

                      <option value selected>SELECT</option>
                      <?php 
                      include_once("../includes/query.php");
                      $getData = getPosition();
                          foreach ($getData as $row) { ?>
                          <option name = "select"> <?php echo $row['pos']; ?>
                          </option>
                      <?php } ?>
                </select> 
                </div><br> 
            </div>

            <div class="form-group row">
                <label for="pagibig" class="col-sm-4 col-form-label">Pag ibig ID</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="pagibig" minlength="12" maxlength="12" title="Input Valid Pagibig ID number" value="<?php if (isset($pagibig)) {if($success != 'true'){echo $pagibig;}}?>">
                </div> <br><br>
                <label for="pagibig" class="col-sm-4 col-form-label">Pag ibig Deduction</label>
                <div class="col-sm-5">
                   <input type="number" name="pagibigDed" style="width: 50%" min="0" value="<?php if (isset($pagibigDed)) {if($success != 'true'){echo $pagibigDed;}}?>">%
                </div>
            </div>

            <div class="form-group row">
                <label for="philhealth" class="col-sm-4 col-form-label">Philhealth ID</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="philhealth" minlength="12" maxlength="12" title="Input Valid Philhealth ID number" value="<?php if (isset($philhealth)) {if($success != 'true'){echo $philhealth;}}?>">
                </div><br><br>
                <label for="philhealthDed" class="col-sm-4 col-form-label">Philhealth Deduction</label>
                <div class="col-sm-5">
                   <input type="number" name="philhealthDed" style="width: 50%" min="0" value="<?php if (isset($philhealthDed)) {if($success != 'true'){echo $philhealthDed;}}?>">%
                </div>
            </div>

            <div class="form-group row">
                <label for="sss" class="col-sm-4 col-form-label">SSS ID<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="sss" minlength="9" maxlength="9" title="Input Valid SSS ID number" required value="<?php if (isset($sss)) {if($success != 'true'){echo $sss;}}?>">
                 </div><br><br>
                <label for="sss" class="col-sm-4 col-form-label">SSS Deduction<span class = "required">*</span></label>
                <div class="col-sm-5">
                   <input type="number" name="sssDed" min="0" style="width: 50%" value="<?php if (isset($sssDed)) {if($success != 'true'){echo $sssDed;}}?>" required>%
                </div>
            </div>

            <div class="form-group row">
                <label for="tin" class="col-sm-4 col-form-label">TIN<span class = "required">*</span></label>
                <div class="col-sm-5">
                 <input type="text" class="form-control" minlength="9" maxlength="9" title="Input Valid TIN" name="tin"  value="<?php if (isset($tin)) {if($success != 'true'){echo $tin;}}?>" require >
                </div><br><br> <label for="tin" class="col-sm-4 col-form-label">TIN Deduction<span class = "required">*</span></label>
                <div class="col-sm-5">
                   <input type="number" name="tinDed" min="0" style="width: 50%" value="<?php if (isset($tinDed)) {if($success != 'true'){echo $tinDed;}}?>" required>%
                </div>
            </div>
  </div>

        <div>
            <center>
            <button class="btn btn-success col-sm-1" name="saveEmployeeBTN"> Register</button>
            <input style="margin-left: 2%;" type="button" class="btn btn-success" onclick="window.location='../staff/superAdmin.php';" value="Cancel"></center>
        </div>

     <div class="inputBox goBack"></div>
    </form>
</center>
</body>
</html>
<?php 
session_start();
include("../includes/button_function.php");
?>

<!DOCTYPE html>
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0" charset = "utf-8">
<html>
    <head>
        <title>Register for an account</title>
        <link rel = "stylesheet" type = "text/css" media = "all" href = "../css/registration.css" />
        <script><link rel="icon" href="../images/zen.ico"></script>
    </head>
    <body>
        <div id = "darkOverlay">
        </div>
        <div class="loginBox">
  
            <form method="POST">
                <div class="inputBox"><br> <br> <br><br>

                    <label>First name<span class = "required">*</span></label>
                    <input type="text" name="fname" required value="<?php if (isset($fname)) {if($success != 'true'){echo $fname;}}?>"><br><br>

                    <label>Middle name<span class = "required"></span></label>
                    <input type="text" name="mname" id="" value="<?php if (isset($mname)) {if($success != 'true'){echo $mname;}}?>"><br><br>

                    <label>Last name<span class = "required">*</span></label>
                    <input type="text" name="lname" id="" required value="<?php if (isset($lname)) {if($success != 'true'){echo $lname;}}?>"><br><br>

                    <label>Username<span class = "required">*</span></label>
                    <input type="text" name="username" id="" required><br><br>

                    <label>Contact number (+63)<span class = "required">*</span></label>
                    <input type="text" name="contact" id="" pattern="[0-9]{10}" title="Please input valid phone number (Philippine Code)" name="contactno" required value="<?php if (isset($contact)) {if($success != 'true'){echo $contact;}}?>"><br><br>

                    <label>Email<span class = "required">*</span></label>
                    <input type="text" name="email" id="" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." name="email"required> <br><br>

                    <label>Password<span class = "required">*</span></label>
                    <input type="password" name="password" id="" minlength="6" title="Password must 6 characters and above" required ><br><br>

                    <label>Confirm Password<span class = "required">*</span></label>
                    <input type="password" name="cpassword" id="" title="Password must 6 characters and above" required ><br><br>

                    <input type="submit" name="registerBTN" value="Register">
                </div>

                <div class="inputBox goBack">
                    <a href = "login.php">
                        Go back
                    </a>
                </div>
            </form>
        </div>
    </body>
</html>
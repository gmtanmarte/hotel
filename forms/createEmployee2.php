<?php 
session_start();
include("../includes/headerAdmin2.php");

 $id = $_SESSION['id']; 

 if (!isset($id)) {
   session_destroy();
   header("location: ../forms/login.php");
 }

?>

<!DOCTYPE html>
<meta name = "viewport" content = "width = device-width, initial-scale = 1.0" charset = "utf-8">
<html>
    <head>
        <title>Register for an account</title>
    </head>
    <style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,700,800');
html body {
  
    margin: 0;
    padding: 0;
    overflow-x: hidden;
    font-family: 'Montserrat', sans-serif;
    font-size: 100%;
    background-color: #EEEEEE;
    color: #333333;
}

</style>
    <body>
        <div id = "darkOverlay">
        </div>
        <div class="loginBox">
          <style>
          .required{
              color : red;
          }
          </style>
 <br><br>
    
                <center>
                <h3>Register Employee</h3>
                <br>
  <div style="width: 50%;">
      <form method="POST">       
            <div class="form-group row">
                <label for="fname" class="col-sm-4 col-form-label">First Name<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="fname" required value="<?php if (isset($fname)) {if($success != 'true'){echo $fname;}}?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="mname" class="col-sm-4 col-form-label">Middle Name<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="mname" id="" value="<?php if (isset($mname)) {if($success != 'true'){echo $mname;}}?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="lname" class="col-sm-4 col-form-label">Last Name<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="text" class="form-control" name="lname" id="" required value="<?php if (isset($lname)) {if($success != 'true'){echo $lname;}}?>">
                </div>
            </div>
            <div class="form-group row">
                <label for="username" class="col-sm-4 col-form-label">Username<span class = "required">*</label>
                <div class="col-sm-5">
                <input type="text" class="form-control" id="username" name="username" require>
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-sm-4 col-form-label">Email<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+­-]+@[a-z0-9.-]+\.[a-­z]{3}$" title="Please input valid email address." name="email"required>
                </div>
            </div>
            <div class="form-group row">
                <label for="contact" class="col-sm-4 col-form-label">Contact number (+63)<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="number" class="form-control" id="contact" name="contact" pattern="[0-9]{10}" title="Please input valid phone number (Philippine Code)" name="contactno" required value="<?php if (isset($contact)) {if($success != 'true'){echo $contact;}}?>"> 
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-4 col-form-label">Password<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="password" class="form-control" id="password" name="password" minlength="6" title="Password must 6 characters and above" required >
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-4 col-form-label">Confirm Password<span class = "required">*</span></label>
                <div class="col-sm-5">
                <input type="password" class="form-control" name="cpassword" id="" title="Password must 6 characters and above" required >
                </div>
            </div>
  </div>
              <br>
  </center>  <center><tr>
                            <td> <button class="btn btn-success col-sm-1" name="registerBTN"> Register</button></td>
                            <td> <br><center><a href = "../staff/superAdmin.php">Cancel</a></center></td>
                        </tr>
                    </table>

                   
                </div>

                <div class="inputBox goBack">
                   
                </div>

  



            </form>
        </div>
    </body>
</html>